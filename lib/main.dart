import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Utility/ControllerBinding.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/Routes.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Map<String, WidgetBuilder> test;

  final userDetail = GetStorage();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: ControllerBinding(),
      title: 'National Tools',
      theme: ThemeData(
        primaryColor: Color(0xffc3252e),
        accentColor: Color(0xff775f34),
        canvasColor: Color(0xff0f0f0f),
        // canvasColor: Color(0xfffcf6ec),
        textTheme: ThemeData.light().textTheme.copyWith(

              bodyText1: TextStyle(color: Color(0xffc3252e)),
              bodyText2: TextStyle(color: Color(0xff775f34)),
              subtitle1: TextStyle(color: Color(0xff775f34)),
              caption: TextStyle(color: Color(0xff775f34)),
              headline1: TextStyle(
                  //headings white
                  fontFamily: 'Roboto',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Color(0xffc3252e)),
              headline2: TextStyle(
                  // NormalOther Text
                  fontFamily: 'Roboto',
                  fontSize: 18,
                  wordSpacing: 4,
                  fontWeight: FontWeight.w600,
                  color: Color(0xff775f34)),
              headline5: TextStyle(
                //headings black
                fontFamily: 'Roboto',
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
              headline3: TextStyle(
                //headings black
                fontFamily: 'Roboto',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(0xff775f34),
              ),
              headline4: TextStyle(
                //headings black
                fontFamily: 'Roboto',
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Color(0xfffcf6ec),
              ),
              headline6: TextStyle(
                //headings black
                fontFamily: 'Roboto',
                fontSize: 14,
                // fontWeight: FontWeight.bold,
                color: Color(0xff775f34),
              ),
              button: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 13,
                // fontWeight: FontWeight.bold,
                color: Color(0xfffcf6ec),
              ),
            ),
        appBarTheme: AppBarTheme(
          color: Theme.of(context).canvasColor,
          centerTitle: true,
          iconTheme: IconThemeData(color: Color(0xffAF0C2A)),
        ),
      ),
      initialRoute: userDetail.read(Finals.TOKEN) != null
          ? Routes.DASHBOARD
          : Routes.BASE_URL,
      getPages: Routes.routes,
    );
  }
}
