import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/Common/model/VariationCode.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class AddVariationPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  VariationController _variationController = Get.find();

  TextEditingController brand = TextEditingController();
  TextEditingController type = TextEditingController();
  TextEditingController hole = TextEditingController();
  TextEditingController size = TextEditingController();
  TextEditingController cutWeight = TextEditingController();
  TextEditingController netWeight = TextEditingController();
  TextEditingController description = TextEditingController();
  var formKey = GlobalKey<FormState>();


  double height;
  double width;


  @override
  Widget build(BuildContext context) {
    height = Get.height;
    width = Get.width * 0.9;
    return Scaffold(
      appBar: _commonWidget.customAppBarWithoutDrawer(
          title: 'Add Variation', context: context),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            height: height * 0.8,
            child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'Add New Variation',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline2,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _commonWidget.buildRectTextField(
                            controller: brand,
                            context: context,
                            width: width * 0.5,
                            hintText: 'Brand',
                            textAlign: TextAlign.start,
                            onValidation: _validationMethods.onBrand),
                        _commonWidget.buildRectTextField(
                            controller: type,
                            context: context,
                            width: width * 0.5,
                            hintText: 'Type',
                            textAlign: TextAlign.start,
                            onValidation: _validationMethods.onType),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _commonWidget.buildRectTextField(
                            controller: size,
                            context: context,
                            width: width * 0.5,
                            hintText: 'Size',
                            textAlign: TextAlign.start,
                            onValidation: _validationMethods.onSize),
                        _commonWidget.buildRectTextField(
                            context: context,
                            controller: hole,
                            textAlign: TextAlign.start,
                            hintText: 'Hole Shape',
                            onValidation: _validationMethods.onHole,
                            width: width * 0.5),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        _commonWidget.buildRectTextField(
                            context: context,
                            controller: cutWeight,
                            textAlign: TextAlign.start,
                            hintText: 'Cut Weight',
                            inputType:
                            TextInputType.numberWithOptions(decimal: true),
                            onValidation: _validationMethods.onWeight,
                            width: width * 0.5),
                        _commonWidget.buildRectTextField(
                            context: context,
                            controller: netWeight,
                            textAlign: TextAlign.start,
                            hintText: 'Net Weight',
                            inputType:
                            TextInputType.numberWithOptions(decimal: true),
                            onValidation: _validationMethods.onWeight,
                            width: width * 0.5),
                      ],

                    ),
                    _commonWidget.buildRectTextArea(
                        context: context,
                        hintText: 'DESCRIPTION',
                        textAlign: TextAlign.start,
                        width: width,
                        onValidation: (val){},
                        controller: description),
                    _commonWidget.buildSquareElevatedButton(
                        context: context, title: 'UPDATE', onTap: onTap),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  void onTap() {
    if (formKey.currentState.validate()) {
      printInfo(info: 'variation update started');
      _variationController.addVariationData(
          brand: brand.text,
          type: type.text,
          size: size.text,
          netWeight: double.parse(netWeight.text),
          cutWeight: double.parse(cutWeight.text),
          holeShape: hole.text,
          description: description.text
      ).then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          brand.clear();
          type.clear();
          hole.clear();
          size.clear();
          cutWeight.clear();
          netWeight.clear();
          description.clear();
        }
      });
    }
  }

}
