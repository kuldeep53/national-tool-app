import 'package:flutter/material.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/Common/model/VariationCode.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/User.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class SearchVariationPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  TextEditingController brand = TextEditingController();
  TextEditingController emailId = TextEditingController();
  ControlPanelController _controlPanelController = Get.find();
  VariationController _variationController = Get.find();

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    height = Get.height;
    width = Get.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: height * 0.28,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      children: [
                        Obx(
                          () => _commonWidget.buildDropDownButton(
                              context: context,
                              labelText: 'Brand',
                              onChange: _variationController.onChangeBrand,
                              typeList: _variationController.brands.toList(),
                              width: width * 0.5),
                        ),
                        Obx(
                          () => _commonWidget.buildDropDownButton(
                              context: context,
                              labelText: 'Type',
                              onChange: _variationController.onChangeType,
                              typeList: _variationController.types.toList(),
                              width: width * 0.5),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Obx(
                          () => _commonWidget.buildDropDownButton(
                              context: context,
                              labelText: 'Size',
                              onChange: _variationController.onChangeSize,
                              typeList: _variationController.sizes.toList(),
                              width: width * 0.5),
                        ),
                        Obx(
                          () => _commonWidget.buildDropDownButton(
                              context: context,
                              labelText: 'Hole Shape',
                              onChange: _variationController.onChangeHole,
                              typeList:
                                  _variationController.holeShapes.toList(),
                              width: width * 0.5),
                        ),
                      ],
                    ),
                    _commonWidget.buildSquareElevatedButton(
                      context: context,
                      onTap: () => _variationController.filterVariationData(),
                      width: width * 0.4,
                    )
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1.5,
              color: Theme.of(context).primaryColor,
            ),
            buildUserDataTable(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Get.offAndToNamed(Routes.ADD_VARIATIONS),
        child: Icon(Icons.add),
        elevation: 20,
      ),
    );
  }

  Container buildUserDataTable() {
    return Container(
      height: height * 0.5,
      child: FutureBuilder<List<VariationCode>>(
          future: _variationController.fetchAllVariationCodes(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.isNotEmpty) {
                return Obx(() {
                  if (_variationController.filterVariations.length <= 0 &&
                      snapshot.data.isNotEmpty) {
                    _variationController.filterVariations
                        .assignAll(snapshot.data);
                  }
                  return _variationController.filterVariations.length <= 0
                      ? Center(
                          child: Text('No User Data Found'),
                        )
                      : ListView(
                          padding: const EdgeInsets.all(16),
                          children: [
                            PaginatedDataTable(
                              showCheckboxColumn: false,
                              rowsPerPage: _variationController
                                          .filterVariations.length <
                                      5
                                  ? _variationController.filterVariations.length
                                  : 5,
                              columns: [
                                DataColumn(label: Text('Brand')),
                                DataColumn(label: Text('Type')),
                                DataColumn(label: Text('Hole')),
                                DataColumn(label: Text('Size')),
                                DataColumn(label: Text('Code')),
                                DataColumn(label: Text('Net Weight')),
                                DataColumn(label: Text('Cut Weight')),
                                DataColumn(label: Text('Created By')),
                                DataColumn(label: Text('Updated By')),
                                DataColumn(label: Text('Update')),
                              ],
                              source: _DataSource(context,
                                  _variationController.filterVariations),
                            ),
                          ],
                        );
                });
              } else {
                return Center(
                  child: Text('No Variation Code Found'),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}

class _Row {
  _Row(this.valueA, this.valueB, this.valueC, this.valueD, this.valueE,
      this.valueF, this.valueG, this.valueH, this.valueI, this.valueJ);

  final String valueA;
  final String valueB;
  final String valueC;
  final String valueD;
  final String valueE;
  final double valueF;
  final double valueG;
  final String valueH;
  final String valueI;
  final String valueJ;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  CommonWidget _commonWidget = CommonWidget();
  VariationController _variationController = Get.find();
  final BuildContext context;
  List<_Row> _rows;
  List<VariationCode> variationList = [];
  int _selectedCount = 0;

  _DataSource(this.context, this.variationList) {
    _rows = variationList
        .map((e) => _Row(
            e.brand,
            e.type,
            e.holeShape,
            e.size,
            e.variationCode,
            e.netWeight,
            e.cutWeight,
            e.createdByName,
            e.updatedByName,
            e.variationCode))
        .toList();
  }

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Text(row.valueA == null ? '' : row.valueA)),
        DataCell(Text(row.valueB == null ? '' : row.valueB)),
        DataCell(Text(row.valueC == null ? '' : row.valueC)),
        DataCell(Text(row.valueD == null ? '' : row.valueD)),
        DataCell(
          Text(
            row.valueE == null ? '' : row.valueE,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        DataCell(Text(row.valueF == null ? '' : row.valueF.toString())),
        DataCell(Text(row.valueG == null ? '' : row.valueG.toString())),
        DataCell(Text(row.valueH == null ? '' : row.valueH)),
        DataCell(Text(row.valueI == null ? '' : row.valueI)),
        DataCell(
          _commonWidget.buildSquareElevatedButton(
              context: context,
              onTap: () => _variationController.getVariationDetail(row.valueJ),
              title: 'Update',
              width: Get.width * 0.17,
              height: Get.height * 0.03,
              textStyle: Theme.of(context).textTheme.button,
              bgColor: Theme.of(context).primaryColor),
        ),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
