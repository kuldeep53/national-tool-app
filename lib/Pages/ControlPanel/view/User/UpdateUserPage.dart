import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/User.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class UpdateUserPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  ControlPanelController _controlPanelController = Get.find();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController address = TextEditingController();
  var formKey = GlobalKey<FormState>();
  final User user = Get.arguments;

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    if (user != null) {
      printInfo(info: '${user.toJson()}');
      setTextFieldValue();
    }
    height = Get.height;
    width = Get.width * 0.9;
    return Scaffold(
      appBar: _commonWidget.customAppBarWithoutDrawer(
          title: 'Update User', context: context),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            height: height * 0.8,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'Update User Page',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  _commonWidget.buildRectTextField(
                      controller: name,
                      context: context,
                      width: width,
                      hintText: 'User Name',
                      textAlign: TextAlign.start,
                      onValidation: _validationMethods.onUserName),
                  _commonWidget.buildRectTextField(
                      controller: email,
                      context: context,
                      width: width,
                      hintText: 'Email Address',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.emailAddress,
                      onValidation: _validationMethods.onEmailAddress),
                  _commonWidget.buildRectTextField(
                      controller: mobile,
                      context: context,
                      width: width,
                      hintText: 'Mobile No',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.number,
                      onValidation: _validationMethods.onMobileNo),
                  _commonWidget.buildRectTextArea(
                      context: context,
                      controller: address,
                      textAlign: TextAlign.start,
                      hintText: 'Address',
                      onValidation: _validationMethods.onAddress,
                      width: width),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        children: [
                          Text(
                            'STATUS',
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          GetBuilder<ControlPanelController>(
                              builder: (controller) {
                            return Switch(
                                inactiveThumbColor:
                                    Theme.of(context).primaryColor,
                                activeColor: Colors.green,
                                value: controller.userStatus.value,
                                onChanged: (val) =>
                                    controller.onChangeStatus(val));
                          }),
                        ],
                      ),
                      _commonWidget.buildSquareElevatedButton(
                          context: context, title: 'UPDATE', onTap: onTap),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void setTextFieldValue() {
    name.text = user.name;
    address.text = user.address;
    email.text = user.emailId;
    mobile.text = user.mobileNo;
  }

  void onTap() {
    if (formKey.currentState.validate()) {
      _controlPanelController
          .updateUser(
              emailId: email.text,
              mobileNo: mobile.text,
              user: user,
              name: name.text,
              address: address.text)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          email.clear();
          mobile.clear();
          name.clear();
          address.clear();
        }
      });
    }
  }
}
