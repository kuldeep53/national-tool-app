import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/User.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class SearchUserPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  TextEditingController mobileNo = TextEditingController();
  TextEditingController emailId = TextEditingController();
  ControlPanelController _controlPanelController = Get.find();

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    height = Get.height;
    width = Get.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: height * 0.2,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _commonWidget.buildRectTextField(
                        context: context,
                        controller: emailId,
                        width: width * 0.93,
                        inputType: TextInputType.emailAddress,
                        hintText: 'Email Address'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        _commonWidget.buildRectTextField(
                            context: context,
                            controller: mobileNo,
                            width: width * 0.5,
                            hintText: 'Mobile No',
                            inputType: TextInputType.phone),
                        _commonWidget.buildSquareElevatedButton(
                          context: context,
                          onTap: () => _controlPanelController.filterUserData(
                              emailId.text, mobileNo.text),
                          width: width * 0.4,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1.5,
              color: Theme.of(context).primaryColor,
            ),
            buildUserDataTable(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Get.offAndToNamed(Routes.ADD_USER),
        child: Icon(Icons.add),
        elevation: 20,
      ),
    );
  }

  Container buildUserDataTable() {
    return Container(
      height: height * 0.5,
      child: FutureBuilder<List<User>>(
          future: _controlPanelController.searchUser(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.isNotEmpty) {
                return GetBuilder<ControlPanelController>(
                    builder: (controller) {
                  if (controller.filterUsers.length <= 0 &&
                      snapshot.data.isNotEmpty) {
                    controller.filterUsers.assignAll(snapshot.data);
                  }
                  return controller.filterUsers.length <= 0
                      ? Center(
                          child: Text('No User Data Found'),
                        )
                      : ListView(
                          padding: const EdgeInsets.all(16),
                          children: [
                            PaginatedDataTable(
                              showCheckboxColumn: false,
                              rowsPerPage: controller.filterUsers.length < 5
                                  ? controller.filterUsers.length
                                  : 5,
                              columns: [
                                DataColumn(label: Text('Name')),
                                DataColumn(label: Icon(Icons.star_half)),
                                DataColumn(label: Text('Mobile No')),
                                DataColumn(label: Text('Email')),
                                DataColumn(label: Text('Assign')),
                                DataColumn(label: Text('update')),
                              ],
                              source:
                                  _DataSource(context, controller.filterUsers),
                            ),
                          ],
                        );
                });
              } else {
                return Center(
                  child: Text('No  User Found'),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}

class _Row {
  _Row(this.valueA, this.valueB, this.valueC, this.valueD, this.valueE,
      this.valueF);

  final String valueA;
  final String valueB;
  final String valueC;
  final String valueD;
  final int valueE;
  final int valueF;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  CommonWidget _commonWidget = CommonWidget();
  ControlPanelController _controlPanelController = Get.find();
  final BuildContext context;
  List<_Row> _rows;
  List<User> processList = [];
  int _selectedCount = 0;

  _DataSource(this.context, this.processList) {
    _rows = processList
        .map((e) =>
            _Row(e.name, e.status, e.mobileNo, e.emailId, e.userId, e.userId))
        .toList();
  }

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Text(row.valueA == null ? '' : row.valueA)),
        DataCell(row.valueB == 'ACTIVE'
            ? Icon(
                Icons.check_circle,
                color: Colors.green,
              )
            : Icon(
                Icons.highlight_off,
                color: Colors.red,
              )),
        DataCell(Text(row.valueC == null ? '' : row.valueC.toString())),
        DataCell(Text(row.valueD == null ? '' : row.valueD)),
        DataCell(
          _commonWidget.buildSquareElevatedButton(
              context: context,
              onTap: () =>_controlPanelController. getMenuDetail(row.valueE),
              title: 'Assign',
              width: Get.width * 0.17,
              height: Get.height * 0.03,
              textStyle: Theme.of(context).textTheme.button,
              bgColor: Theme.of(context).primaryColor),
        ),
        DataCell(
          _commonWidget.buildSquareElevatedButton(
              context: context,
              onTap: () => _controlPanelController.getUserDetails(row.valueE),
              title: 'Update',
              width: Get.width * 0.17,
              height: Get.height * 0.03,
              textStyle: Theme.of(context).textTheme.button,
              bgColor: Theme.of(context).primaryColor),
        ),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
