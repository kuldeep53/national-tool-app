import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class AddUserPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  ControlPanelController _controlPanelController = Get.find();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController address = TextEditingController();
  var formKey = GlobalKey<FormState>();

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    height = Get.height;
    width = Get.width * 0.9;
    return Scaffold(
        appBar: _commonWidget.customAppBarWithoutDrawer(
            title: 'Register User', context: context),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              height: height * 0.8,
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      'New User Register Page',
                      style: Theme.of(context).textTheme.headline2,
                    ),
                    _commonWidget.buildRectTextField(
                        controller: name,
                        context: context,
                        width: width,
                        hintText: 'User Name',
                        textAlign: TextAlign.start,
                        onValidation: _validationMethods.onUserName),
                    _commonWidget.buildRectTextField(
                        controller: email,
                        context: context,
                        width: width,
                        hintText: 'Email Address',
                        textAlign: TextAlign.start,
                        inputType: TextInputType.emailAddress,
                        onValidation: _validationMethods.onEmailAddress),
                    _commonWidget.buildRectTextField(
                        controller: password,
                        context: context,
                        width: width,
                        hintText: 'Password',
                        textAlign: TextAlign.start,
                        onValidation: _validationMethods.onPassword),
                    _commonWidget.buildRectTextField(
                        controller: mobile,
                        context: context,
                        width: width,
                        hintText: 'Mobile No',
                        textAlign: TextAlign.start,
                        inputType: TextInputType.number,
                        onValidation: _validationMethods.onMobileNo),
                    _commonWidget.buildRectTextArea(
                        context: context,
                        controller: address,
                        textAlign: TextAlign.start,
                        hintText: 'Address',
                        onValidation: _validationMethods.onAddress,
                        width: width),
                    _commonWidget.buildSquareElevatedButton(
                        context: context, title: 'SUBMIT', onTap: onTap)
                  ],
                ),
              ),
            ),
          ),
        ),
    );
  }


  void onTap() {
    if (formKey.currentState.validate()) {
      _controlPanelController
          .userRegister(
              emailId: email.text,
              mobileNo: mobile.text,
              password: password.text,
              name: name.text,
              address: address.text)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          email.clear();
          mobile.clear();
          name.clear();
          address.clear();
          password.clear();
        }
      });
    }
  }
}
