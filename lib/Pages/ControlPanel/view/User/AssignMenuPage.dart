import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';

class AssignMenuPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ControlPanelController _controlPanelController = ControlPanelController();

  int userId = Get.arguments;

  @override
  Widget build(BuildContext context) {
    printInfo(info: '${userId}');
    return Scaffold(
      appBar: _commonWidget.customAppBarWithoutDrawer(
          title: 'Access Control Panel',
          context: context,
          action: [
            IconButton(
                onPressed: () => _controlPanelController.setUserMenus(userId),
                icon: Icon(Icons.save))
          ]),
      body: FutureBuilder<Set<String>>(
          future: _controlPanelController.getAssignedMenus(userId),
          builder: (context, snapshot) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: CustomScrollView(
                  slivers: getCheckBoxWidget(context, snapshot.data)),
            );
          }),
    );
  }

  List<Widget> getCheckBoxWidget(
      BuildContext context, Set<String> assignMenus) {
    List<Widget> widgetList = [];
    _controlPanelController.getMenuList().forEach((menu) {
      widgetList.add(SliverToBoxAdapter(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  menu.menus.menuName.toUpperCase(),
                  style: Theme.of(context).textTheme.headline2,
                ),
                Obx(
                  () => Theme(
                    data: Theme.of(context).copyWith(
                      unselectedWidgetColor: Colors.white,
                    ),
                    child: Checkbox(
                      onChanged: (bool value) => _controlPanelController
                          .onParentCheckBox(value, menu.menus.menuCode),
                      value: _controlPanelController.menuList
                          .contains(menu.menus.menuCode),
                    ),
                  ),
                )
              ],
            ),
            Divider(
              thickness: 2,
              color: Theme.of(context).primaryColor,
            )
          ],
        ),
      ));
      widgetList.add(SliverGrid(
        delegate: SliverChildBuilderDelegate(
          (context, index) {
            return Align(
              child: Row(
                children: [
                  Obx(
                    () => Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: Checkbox(
                          value: _controlPanelController.menuList
                              .contains(menu.childMenus[index].menuCode),
                          onChanged: (val) =>
                              _controlPanelController.onChildCheckBox(
                                  val, menu.childMenus[index].menuCode)),
                    ),
                  ),
                  FittedBox(
                    child: Text(menu.childMenus[index].menuName.toUpperCase()
                        ,),
                  ),
                ],
              ),
            );
          },
          childCount: menu.childMenus.length,
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          mainAxisSpacing: 15,
          crossAxisSpacing: 10,
          childAspectRatio: 2.0,
        ),
      ));
    });
    return widgetList;
  }
}
