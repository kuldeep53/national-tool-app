import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/Suppliers.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class UpdateSupplierPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  ControlPanelController _controlPanelController = Get.find();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController description = TextEditingController();
  TextEditingController city = TextEditingController();
  var formKey = GlobalKey<FormState>();
  final Suppliers supplier = Get.arguments;

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    if (supplier != null) {
      printInfo(info: '${supplier.toJson()}');
      setTextFieldValue();
    }
    height = Get.height;
    width = Get.width * 0.9;
    return Scaffold(
      appBar: _commonWidget.customAppBarWithoutDrawer(
          title: 'Update Supplier', context: context),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            height: height * 0.8,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'Update Supplier Page',
                    style: Theme
                        .of(context)
                        .textTheme
                        .headline2,
                  ),
                  _commonWidget.buildRectTextField(
                      controller: name,
                      context: context,
                      width: width,
                      hintText: 'Supplier Name',
                      textAlign: TextAlign.start,
                      onValidation: _validationMethods.onUserName),
                  _commonWidget.buildRectTextField(
                      controller: email,
                      context: context,
                      width: width,
                      hintText: 'Email Address',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.emailAddress,
                      onValidation: _validationMethods.onEmailAddress),
                  _commonWidget.buildRectTextField(
                      controller: mobile,
                      context: context,
                      width: width,
                      hintText: 'Mobile No',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.number,
                      onValidation: _validationMethods.onMobileNo),
                  _commonWidget.buildRectTextField(
                      controller: city,
                      context: context,
                      width: width,
                      hintText: 'City',
                      textAlign: TextAlign.start,
                      onValidation: _validationMethods.onCity),
                  _commonWidget.buildRectTextArea(
                      context: context,
                      controller: description,
                      textAlign: TextAlign.start,
                      hintText: 'description',
                      onValidation: (val) {},
                      width: width),
                  _commonWidget.buildSquareElevatedButton(
                      context: context, title: 'UPDATE', onTap: onTap),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void setTextFieldValue() {
    name.text = supplier.name;
    description.text = supplier.description;
    email.text = supplier.emailId;
    mobile.text = supplier.mobileNo;
    city.text = supplier.city;
  }

  void onTap() {
    if (formKey.currentState.validate()) {
      _controlPanelController
          .updateSupplier(
          emailId: email.text,
          mobileNo: mobile.text,
          supplier: supplier,
          name: name.text,
          description: description.text,
          city: city.text)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          email.clear();
          mobile.clear();
          name.clear();
          description.clear();
          city.clear();
        }
      });
    }
  }
}
