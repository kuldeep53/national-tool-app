import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class AddSupplierPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  ControlPanelController _controlPanelController = Get.find();

  TextEditingController name = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController city = TextEditingController();
  TextEditingController mobile = TextEditingController();
  TextEditingController description = TextEditingController();
  var formKey = GlobalKey<FormState>();

  double height;
  double width;

  @override
  Widget build(BuildContext context) {
    height = Get.height;
    width = Get.width * 0.9;
    return Scaffold(
      appBar: _commonWidget.customAppBarWithoutDrawer(
          title: 'Register Supplier', context: context),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            height: height * 0.8,
            child: Form(
              key: formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    'New Supplier Register Page',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                  _commonWidget.buildRectTextField(
                      controller: name,
                      context: context,
                      width: width,
                      hintText: 'Supplier Name',
                      textAlign: TextAlign.start,
                      onValidation: _validationMethods.onUserName),
                  _commonWidget.buildRectTextField(
                      controller: email,
                      context: context,
                      width: width,
                      hintText: 'Email Address',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.emailAddress,
                      onValidation: _validationMethods.onEmailAddress),
                  _commonWidget.buildRectTextField(
                      controller: city,
                      context: context,
                      width: width,
                      hintText: 'City',
                      textAlign: TextAlign.start,
                      onValidation: _validationMethods.onPassword),
                  _commonWidget.buildRectTextField(
                      controller: mobile,
                      context: context,
                      width: width,
                      hintText: 'Mobile No',
                      textAlign: TextAlign.start,
                      inputType: TextInputType.number,
                      onValidation: _validationMethods.onMobileNo),
                  _commonWidget.buildRectTextArea(
                      context: context,
                      controller: description,
                      textAlign: TextAlign.start,
                      hintText: 'Description',
                      width: width,onValidation: (val){}),
                  _commonWidget.buildSquareElevatedButton(
                      context: context, title: 'SUBMIT', onTap: onTap)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  void onTap() {
    if (formKey.currentState.validate()) {
      _controlPanelController
          .addSupplier(
          emailId: email.text,
          mobileNo: mobile.text,
          city: city.text,
          name: name.text,
          description: description.text)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          email.clear();
          mobile.clear();
          name.clear();
          description.clear();
          city.clear();
        }
      });
    }
  }
}
