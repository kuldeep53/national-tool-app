import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';

class ControlBottomTabBar extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();

  ControlPanelController _controlPanelController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _commonWidget.customAppBar(
        title: 'CONTROL PANEL',
        context: context,
      ),
      drawer: CustomDrawer('CONTROL'),
      body: Obx(() => _controlPanelController
          .pages[_controlPanelController.tabIndex.value]),
      bottomNavigationBar: GetBuilder<ControlPanelController>(
        builder: (controller) {
          return BottomNavigationBar(
            currentIndex: controller.tabIndex.value,
            type: BottomNavigationBarType.fixed,
            backgroundColor: Theme.of(context).primaryColor,
            selectedItemColor: Colors.white,
            selectedFontSize: 16,
            onTap: controller.updateTabIndex,
            items: controller.tabsIcon,
          );
        },
      ),
    );
  }
}
