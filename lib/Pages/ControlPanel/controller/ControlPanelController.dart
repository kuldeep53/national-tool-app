import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/model/CommonReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/AddSupplierReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/GetUserMenuReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/SetUserMenuReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/Suppliers.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/UpdateUserReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/User.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/UserRegisterReqBean.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/UserSearchReqBean.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class ControlPanelController extends GetxController {
  final userDetails = GetStorage();
  RxInt tabIndex = RxInt(0);

  static List<User> userData = [];
  RxList<User> filterUsers = RxList([]);

  RxBool userStatus = RxBool(false);
  RxSet<String> menuList = RxSet({});

  static List<Suppliers> supplierData = [];
  RxList<Suppliers> filterSuppliers = RxList([]);

  Future<List<Suppliers>> searchSupplier() async {
    CommonReqBean reqBean = CommonReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_SUPPLIER, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      supplierData = suppliersFromJson(response.data);
      filterSuppliers.assignAll(supplierData);
      return supplierData;
    } else {
      return [];
    }
  }

  Future<bool> addSupplier(
      {String name,
      String emailId,
      String city,
      String mobileNo,
      String description}) async {
    AddSupplierReqBean reqBean = AddSupplierReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN),
        mobileNo: mobileNo,
        emailId: emailId,
        description: description,
        city: city,
        name: name);
    ResponseBean response = await Apis.callPostService(
        url: Apis.ADD_SUPPLIER, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> updateSupplier(
      {String name,
      String emailId,
      String city,
      String mobileNo,
      String description,Suppliers supplier}) async {
    if(!(name==supplier.name&&emailId==supplier.emailId&&city==supplier
        .city&&mobileNo==supplier.mobileNo&&description==supplier.description)) {
      AddSupplierReqBean reqBean = AddSupplierReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN),
        suppId: supplier.suppId,
        mobileNo: mobileNo!=supplier.mobileNo?mobileNo:null,
        emailId: emailId!=supplier.emailId?emailId:null,
        description: description!=supplier.description?description:null,
        city: city!=supplier.city?city:null,
        name: name!=supplier.name?name:null,
      );
      ResponseBean response = await Apis.callPostService(
          url: Apis.UPDATE_SUPPLIER, reqBody: reqBean.toJson());
      if (response != null && response.responseCode == 0) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  Future<bool> setUserMenus(int userId) async {
    SetUserMenusReqBean reqBean = SetUserMenusReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN),
        userId: userId,
        menuCodes: menuList.toList());
    ResponseBean response = await Apis.callPostService(
        url: Apis.ASSIGN_MENU_TO_USER, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0)
      return true;
    else
      return false;
  }

  void onParentCheckBox(bool val, String menuCode) {
    List<String> childMenus = getMenuList()
        .firstWhere((element) => element.menus.menuCode == menuCode)
        .childMenus
        .map((e) => e.menuCode)
        .toList();
    childMenus.add(menuCode);
    if (val == true)
      menuList.addAll(childMenus);
    else
      menuList.removeWhere((element) => childMenus.contains(element));
    update();
  }

  void onChildCheckBox(bool val, String menuCode) {
    if (val == true)
      menuList.add(menuCode);
    else
      menuList.removeWhere((element) => element == menuCode);
    update();
  }

  List<UserMenu> getMenuList() {
    return userMenuFromJson(userDetails.read(Finals.MENUS));
  }

  Future<Set<String>> getAssignedMenus(int userId) async {
    GetUserMenusReqBean reqBean = GetUserMenusReqBean(
      token: userDetails.read(Finals.TOKEN),
      userId: userId,
      loginUserId: userDetails.read(Finals.USER_ID),
    );
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_ASSIGNED_MENUS, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      menuList.assignAll(List<String>.from(response.data));
      update();
      return menuList;
    } else
      return {};
  }

  Future<bool> updateUser(
      {String mobileNo,
      @required User user,
      String address,
      String name,
      String emailId}) async {
    String status = userStatus.value ? 'ACTIVE' : 'INACTIVE';
    if (!(mobileNo == user.mobileNo &&
        address == user.address &&
        name == user.name &&
        emailId == user.emailId &&
        status == user.status)) {
      UserUpdateReqBean reqBean = UserUpdateReqBean(
          userId: user.userId,
          loginUserId: userDetails.read(Finals.USER_ID),
          token: userDetails.read(Finals.TOKEN),
          mobileNo: mobileNo != user.mobileNo ? mobileNo : null,
          address: address != user.address ? address : null,
          name: name != user.name ? name : null,
          emailId: emailId != user.emailId ? emailId : null,
          status: status != user.status ? status : null);

      ResponseBean response = await Apis.callPostService(
          url: Apis.UPDATE_USER, reqBody: reqBean.toJson());
      if (response != null && response.responseCode == 0)
        return true;
      else
        return false;
    }
    return true;
  }

  void filterUserData(String emailId, String mobileNo) {
    if (mobileNo.isNotEmpty)
      filterUsers.assignAll(
          userData.where((element) => element.mobileNo == mobileNo).toList());
    if (emailId.isNotEmpty)
      filterUsers.assignAll(
          userData.where((element) => element.emailId == emailId).toList());
    if (emailId.isEmpty && mobileNo.isEmpty) filterUsers.assignAll(userData);
    update();
  }

  void filterSupplierData(String emailId, String mobileNo, String name) {
    if (mobileNo.isNotEmpty)
      filterSuppliers.assignAll(supplierData
          .where((element) => element.mobileNo == mobileNo)
          .toList());
    if (emailId.isNotEmpty)
      filterSuppliers.assignAll(
          supplierData.where((element) => element.emailId == emailId).toList());
    if (name.isNotEmpty)
      filterSuppliers.assignAll(
          supplierData.where((element) => element.name == emailId).toList());
    if (emailId.isEmpty && mobileNo.isEmpty && name.isEmpty)
      filterSuppliers.assignAll(supplierData);
    update();
  }

  void getUserDetails(int userId) {
    User user = userData.firstWhere((element) => element.userId == userId);
    userStatus.value = user.status == 'ACTIVE' ? true : false;
    Get.offAndToNamed(Routes.UPDATE_USER, arguments: user);
  }

  void getSupplierDetails(int suppId) {
    Suppliers supplier = supplierData.firstWhere((element) => element.suppId ==
        suppId);
    Get.offAndToNamed(Routes.UPDATE_SUPPLIER, arguments: supplier);
  }

  void getMenuDetail(int userId) {
    Get.offAndToNamed(Routes.ASSIGN_MENUS, arguments: userId);
  }

  void onChangeStatus(bool val) {
    userStatus.value = val;
    update();
  }

  Future<List<User>> searchUser() async {
    UserSearchReqBean reqBean = UserSearchReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.SEARCH_USER, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      userData = userFromJson(response.data);
      filterUsers.assignAll(userData);
      return userData;
    } else {
      return [];
    }
  }

  Future<bool> userRegister(
      {@required String mobileNo,
      @required String password,
      @required String address,
      @required String name,
      @required String emailId}) async {
    UserRegisterReqBean reqBean = UserRegisterReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN),
        address: address,
        mobileNo: mobileNo,
        password: password,
        name: name,
        emailId: emailId);
    ResponseBean response = await Apis.callPostService(
        url: Apis.REGISTER_USER, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0)
      return true;
    else
      return false;
  }

  void updateTabIndex(value) {
    tabIndex.value = value;
    update();
  }

  List<String> fetchAllControlMenuCodes() {
    return userMenuFromJson(userDetails.read(Finals.MENUS))
        .firstWhere(
            (element) => element.menus.menuCode.toUpperCase() == 'CONTROL')
        .childMenus
        .map((e) {
          if (e.sequence != 0) return e.menuName;
        })
        .where((element) => element != null)
        .toList();
  }

  List<Widget> get pages => fetchAllControlMenuCodes()
      .map((e) => Finals.controlPanelTabs[e])
      .where((element) => element != null)
      .toList();

  List<BottomNavigationBarItem> get tabsIcon => fetchAllControlMenuCodes()
      .map((e) => BottomNavigationBarItem(
            label: Finals.controlPanelIconMap[e] != null
                ? Finals.controlPanelTabName[e]
                : "",
            icon: Icon(Finals.controlPanelIconMap[e]),
          ))
      .where((element) => element.label.isNotEmpty)
      .toList();
}
