
// To parse this JSON data, do
//
//     final userRegisterReqBean = userRegisterReqBeanFromJson(jsonString);

import 'dart:convert';

UserRegisterReqBean userRegisterReqBeanFromJson(String str) => UserRegisterReqBean.fromJson(json.decode(str));

String userRegisterReqBeanToJson(UserRegisterReqBean data) => json.encode(data.toJson());

class UserRegisterReqBean {
  UserRegisterReqBean({
    this.address,
    this.device='ANDROID',
    this.emailId,
    this.loginUserId,
    this.mobileNo,
    this.name,
    this.password,
    this.token,
    this.userType='ADMIN',
  });

  String address;
  String device;
  String emailId;
  int loginUserId;
  String mobileNo;
  String name;
  String password;
  String token;
  String userType;

  factory UserRegisterReqBean.fromJson(Map<String, dynamic> json) => UserRegisterReqBean(
    address: json["address"],
    device: json["device"],
    emailId: json["emailId"],
    loginUserId: json["loginUserId"],
    mobileNo: json["mobileNo"],
    name: json["name"],
    password: json["password"],
    token: json["token"],
    userType: json["userType"],
  );

  Map<String, dynamic> toJson() => {
    "address": address,
    "device": device,
    "emailId": emailId,
    "loginUserId": loginUserId,
    "mobileNo": mobileNo,
    "name": name,
    "password": password,
    "token": token,
    "userType": userType,
  };
}
