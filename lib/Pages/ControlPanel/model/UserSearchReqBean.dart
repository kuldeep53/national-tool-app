// To parse this JSON data, do
//
//     final userSearchReqBean = userSearchReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

UserSearchReqBean userSearchReqBeanFromJson(String str) => UserSearchReqBean.fromJson(json.decode(str));

String userSearchReqBeanToJson(UserSearchReqBean data) => json.encode(data.toJson());

class UserSearchReqBean {
  UserSearchReqBean({
    this.device='ANDROID',
    @required this.loginUserId,
    this.mobileNo,
    this.name,
    this.password,
    this.status,
    @required this.token,
    this.userId,
    this.userType='ADMIN',
  });

  String device;
  int loginUserId;
  String mobileNo;
  String name;
  String password;
  String status;
  String token;
  int userId;
  String userType;

  factory UserSearchReqBean.fromJson(Map<String, dynamic> json) => UserSearchReqBean(
    device: json["device"],
    loginUserId: json["loginUserId"],
    mobileNo: json["mobileNo"],
    name: json["name"],
    password: json["password"],
    status: json["status"],
    token: json["token"],
    userId: json["userId"],
    userType: json["userType"],
  );

  Map<String, dynamic> toJson() => {
    "device": device,
    "loginUserId": loginUserId,
    "mobileNo": mobileNo,
    "name": name,
    "password": password,
    "status": status,
    "token": token,
    "userId": userId,
    "userType": userType,
  };
}
