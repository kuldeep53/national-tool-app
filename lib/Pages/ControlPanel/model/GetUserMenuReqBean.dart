// To parse this JSON data, do
//
//     final assignedUserMenusReqBean = assignedUserMenusReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

GetUserMenusReqBean assignedUserMenusReqBeanFromJson(String str) =>
    GetUserMenusReqBean.fromJson(json.decode(str));

String assignedUserMenusReqBeanToJson(GetUserMenusReqBean data) =>
    json.encode(data.toJson());

class GetUserMenusReqBean {
  GetUserMenusReqBean({
    this.device = 'ANDROID',
    @required this.loginUserId,
    @required this.token,
    @required this.userId,
  });

  String device;
  int loginUserId;
  List<String> menuCodes;
  String token;
  int userId;

  factory GetUserMenusReqBean.fromJson(Map<String, dynamic> json) =>
      GetUserMenusReqBean(
        device: json["device"],
        loginUserId: json["loginUserId"],
        token: json["token"],
        userId: json["userId"],
      );

  Map<String, dynamic> toJson() => {
        "device": device,
        "loginUserId": loginUserId,
        "token": token,
        "userId": userId,
      };
}
