// To parse this JSON data, do
//
//     final addVariationReqBean = addVariationReqBeanFromJson(jsonString);

import 'dart:convert';

AddVariationReqBean addVariationReqBeanFromJson(String str) => AddVariationReqBean.fromJson(json.decode(str));

String addVariationReqBeanToJson(AddVariationReqBean data) => json.encode(data.toJson());

class AddVariationReqBean {
  AddVariationReqBean({
    this.brand,
    this.description,
    this.device="ANDROID",
    this.holeShape,
    this.loginUserId,
    this.size,
    this.token,
    this.type,
    this.variationId,
    this.cutWeight,
    this.netWeight,
  });

  String brand;
  String description;
  String device;
  String holeShape;
  int loginUserId;
  String size;
  String token;
  String type;
  int variationId;
  double cutWeight;
  double netWeight;

  factory AddVariationReqBean.fromJson(Map<String, dynamic> json) => AddVariationReqBean(
    brand: json["brand"],
    description: json["description"],
    device: json["device"],
    holeShape: json["holeShape"],
    loginUserId: json["loginUserId"],
    size: json["size"],
    token: json["token"],
    type: json["type"],
    variationId: json["variationId"],
    cutWeight: json["cutWeight"],
      netWeight:json["netWeight"]
  );

  Map<String, dynamic> toJson() => {
    "brand": brand,
    "description": description,
    "device": device,
    "holeShape": holeShape,
    "loginUserId": loginUserId,
    "size": size,
    "token": token,
    "type": type,
    "variationId": variationId,
    "cutWeight": cutWeight,
    "netWeight":netWeight,
  };
}
