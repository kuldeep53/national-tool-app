// To parse this JSON data, do
//
//     final assignedUserMenusReqBean = assignedUserMenusReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

String assignedUserMenusReqBeanToJson(SetUserMenusReqBean data) =>
    json.encode(data.toJson());

class SetUserMenusReqBean {
  SetUserMenusReqBean({
    this.device = 'ANDROID',
    @required this.menuCodes,
    @required this.loginUserId,
    @required this.token,
    @required this.userId,
  });

  String device;
  int loginUserId;
  List<String> menuCodes;
  String token;
  int userId;

  Map<String, dynamic> toJson() => {
        "device": device,
        "loginUserId": loginUserId,
        "menuCodes": List<dynamic>.from(menuCodes.map((x) => x)),
        "token": token,
        "userId": userId,
      };
}
