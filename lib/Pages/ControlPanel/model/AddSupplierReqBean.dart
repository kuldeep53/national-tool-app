// To parse this JSON data, do
//
//     final addSupplierReqBean = addSupplierReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

AddSupplierReqBean addSupplierReqBeanFromJson(String str) =>
    AddSupplierReqBean.fromJson(json.decode(str));

String addSupplierReqBeanToJson(AddSupplierReqBean data) =>
    json.encode(data.toJson());

class AddSupplierReqBean {
  AddSupplierReqBean({
    @required this.city,
    this.description,
    this.device = 'ANDROID',
    @required this.emailId,
    @required this.loginUserId,
    @required this.mobileNo,
    @required this.name,
    this.suppId,
    @required this.token,
  });

  String city;
  String description;
  String device;
  String emailId;
  int loginUserId;
  String mobileNo;
  String name;
  int suppId;
  String token;

  factory AddSupplierReqBean.fromJson(Map<String, dynamic> json) =>
      AddSupplierReqBean(
        city: json["city"],
        description: json["description"],
        device: json["device"],
        emailId: json["emailId"],
        loginUserId: json["loginUserId"],
        mobileNo: json["mobileNo"],
        name: json["name"],
        suppId: json["suppId"],
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "city": city,
        "description": description,
        "device": device,
        "emailId": emailId,
        "loginUserId": loginUserId,
        "mobileNo": mobileNo,
        "name": name,
        "suppId": suppId,
        "token": token,
      };
}
