// To parse this JSON data, do
//
//     final userUpdateReqBean = userUpdateReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/cupertino.dart';

UserUpdateReqBean userUpdateReqBeanFromJson(String str) =>
    UserUpdateReqBean.fromJson(json.decode(str));

String userUpdateReqBeanToJson(UserUpdateReqBean data) =>
    json.encode(data.toJson());

class UserUpdateReqBean {
  UserUpdateReqBean({
    this.address,
    this.device = 'ANDROID',
    this.emailId,
    @required this.loginUserId,
    this.mobileNo,
    this.name,
    this.password,
    this.status,
    @required this.token,
    @required this.userId,
    this.userType = 'ADMIN',
  });

  String address;
  String device;
  String emailId;
  int loginUserId;
  String mobileNo;
  String name;
  String password;
  String status;
  String token;
  int userId;
  String userType;

  factory UserUpdateReqBean.fromJson(Map<String, dynamic> json) =>
      UserUpdateReqBean(
        address: json["address"],
        device: json["device"],
        emailId: json["emailId"],
        loginUserId: json["loginUserId"],
        mobileNo: json["mobileNo"],
        name: json["name"],
        password: json["password"],
        status: json["status"],
        token: json["token"],
        userId: json["userId"],
        userType: json["userType"],
      );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "address": address,
      "device": device,
      "emailId": emailId,
      "loginUserId": loginUserId,
      "mobileNo": mobileNo,
      "name": name,
      "password": password,
      "status": status,
      "token": token,
      "userId": userId,
      "userType": userType,
    };
    map.removeWhere((key, value) => key == null || value == null);
    return map;
  }
}
