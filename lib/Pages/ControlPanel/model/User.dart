// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

List<User> userFromJson(List<dynamic> str) =>
    List<User>.from(str.map((x) => User.fromJson(x)));

String userToJson(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  User({
    this.userId,
    this.name,
    this.mobileNo,
    this.emailId,
    this.address,
    this.userType,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int userId;
  String name;
  String mobileNo;
  String emailId;
  String address;
  String userType;
  String status;
  String createdAt;
  String updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json["userId"],
        name: json["name"],
        mobileNo: json["mobileNo"],
        emailId: json["emailId"],
        address: json["address"],
        userType: json["userType"],
        status: json["status"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
      );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> map = {
      "userId": userId,
      "name": name,
      "mobileNo": mobileNo,
      "emailId": emailId,
      "address": address,
      "userType": userType,
      "status": status,
      "createdAt": createdAt,
      "updatedAt": updatedAt,
    };
    return map;
  }
}
