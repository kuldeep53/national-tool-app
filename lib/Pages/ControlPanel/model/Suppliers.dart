// To parse this JSON data, do
//
//     final suppliers = suppliersFromJson(jsonString);

import 'dart:convert';

List<Suppliers> suppliersFromJson(List<dynamic> str) =>
    List<Suppliers>.from(str.map((x) => Suppliers.fromJson(x)));

String suppliersToJson(List<Suppliers> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Suppliers {
  Suppliers({
    this.suppId,
    this.name,
    this.city,
    this.mobileNo,
    this.emailId,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
  });

  int suppId;
  String name;
  String city;
  String mobileNo;
  String emailId;
  String description;
  int createdAt;
  int updatedAt;
  int createdBy;
  int updatedBy;

  factory Suppliers.fromJson(Map<String, dynamic> json) => Suppliers(
        suppId: json["suppId"],
        name: json["name"],
        city: json["city"],
        mobileNo: json["mobileNo"],
        emailId: json["emailId"],
        description: json["description"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
      );

  Map<String, dynamic> toJson() => {
        "suppId": suppId,
        "name": name,
        "city": city,
        "mobileNo": mobileNo,
        "emailId": emailId,
        "description": description,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "createdBy": createdBy,
        "updatedBy": updatedBy,
      };
}
