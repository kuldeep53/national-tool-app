// To parse this JSON data, do
//
//     final processHistoryReqBean = processHistoryReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/foundation.dart';

ProcessHistoryReqBean processHistoryReqBeanFromJson(String str) =>
    ProcessHistoryReqBean.fromJson(json.decode(str));

String processHistoryReqBeanToJson(ProcessHistoryReqBean data) =>
    json.encode(data.toJson());

class ProcessHistoryReqBean {
  ProcessHistoryReqBean({
    this.device = 'ANDROID',
    @required this.fromDate,
    @required this.loginUserId,
    @required this.toDate,
    @required this.token,
    this.stateCode,
  });

  String device;
  DateTime fromDate;
  int loginUserId;
  DateTime toDate;
  String token;
  String stateCode;

  factory ProcessHistoryReqBean.fromJson(Map<String, dynamic> json) =>
      ProcessHistoryReqBean(
        device: json["device"],
        fromDate: DateTime.parse(json["fromDate"]),
        loginUserId: json["loginUserId"],
        toDate: DateTime.parse(json["toDate"]),
        token: json["token"],
        stateCode: json["stateCode"],
      );

  Map<String, dynamic> toJson() => {
        "device": device,
        "fromDate":
            "${fromDate.year.toString().padLeft(4, '0')}-${fromDate.month.toString().padLeft(2, '0')}-${fromDate.day.toString().padLeft(2, '0')}",
        "loginUserId": loginUserId,
        "toDate":
            "${toDate.year.toString().padLeft(4, '0')}-${toDate.month.toString().padLeft(2, '0')}-${toDate.day.toString().padLeft(2, '0')}",
        "token": token,
        "stateCode": stateCode,
      };
}
