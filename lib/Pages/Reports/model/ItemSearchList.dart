// To parse this JSON data, do
//
//     final itemSearch = itemSearchFromJson(jsonString);

import 'dart:convert';

List<ItemSearch> itemSearchFromJson(List<dynamic> str) =>
    List<ItemSearch>.from(str.map((x) => ItemSearch.fromJson(x)));

String itemSearchToJson(List<ItemSearch> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ItemSearch {
  ItemSearch({
    this.variationCode,
    this.inProgress,
    this.finished,
    this.rejected,
    this.srapping,
    this.order,
    this.stateDetails,
  });

  String variationCode;
  int inProgress;
  int finished;
  int rejected;
  int srapping;
  int order;
  List<StateDetail> stateDetails;

  factory ItemSearch.fromJson(Map<String, dynamic> json) => ItemSearch(
        variationCode: json["variationCode"],
        inProgress: json["inProgress"],
        finished: json["finished"],
        rejected: json["rejected"],
        srapping: json["srapping"],
        order: json["order"],
        stateDetails: List<StateDetail>.from(
            json["stateDetails"].map((x) => StateDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "variationCode": variationCode,
        "inProgress": inProgress,
        "finished": finished,
        "rejected": rejected,
        "srapping": srapping,
        "order": order,
        "stateDetails": List<dynamic>.from(stateDetails.map((x) => x.toJson())),
      };
}

class StateDetail {
  StateDetail({
    this.stateName,
    this.inProgress,
  });

  String stateName;
  int inProgress;

  factory StateDetail.fromJson(Map<String, dynamic> json) => StateDetail(
        stateName: json["stateName"],
        inProgress: json["inProgress"],
      );

  Map<String, dynamic> toJson() => {
        "stateName": stateName,
        "inProgress": inProgress,
      };
}
