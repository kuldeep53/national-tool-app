// To parse this JSON data, do
//
//     final itemSearchReqBean = itemSearchReqBeanFromJson(jsonString);

import 'dart:convert';

ItemSearchReqBean itemSearchReqBeanFromJson(String str) => ItemSearchReqBean.fromJson(json.decode(str));

String itemSearchReqBeanToJson(ItemSearchReqBean data) => json.encode(data.toJson());

class ItemSearchReqBean {
  ItemSearchReqBean({
    this.device="ANDROID",
    this.fromDate,
    this.loginUserId,
    this.toDate,
    this.token,
    this.variationCode,
  });

  String device;
  DateTime fromDate;
  int loginUserId;
  DateTime toDate;
  String token;
  String variationCode;

  factory ItemSearchReqBean.fromJson(Map<String, dynamic> json) => ItemSearchReqBean(
    device: json["device"],
    fromDate: DateTime.parse(json["fromDate"]),
    loginUserId: json["loginUserId"],
    toDate: DateTime.parse(json["toDate"]),
    token: json["token"],
    variationCode: json["variationCode"],
  );

  Map<String, dynamic> toJson() => {
    "device": device,
    "fromDate":
    "${fromDate.year.toString().padLeft(4, '0')}-${fromDate.month.toString().padLeft(2, '0')}-${fromDate.day.toString().padLeft(2, '0')}",
    "loginUserId": loginUserId,
    "toDate":
    "${toDate.year.toString().padLeft(4, '0')}-${toDate.month.toString().padLeft(2, '0')}-${toDate.day.toString().padLeft(2, '0')}",
    "token": token,
    "variationCode": variationCode,
  };
}
