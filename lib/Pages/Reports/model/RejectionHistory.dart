// To parse this JSON data, do
//
//     final rejectionHistory = rejectionHistoryFromJson(jsonString);

import 'dart:convert';

List<RejectionHistory> rejectionHistoryFromJson(List<dynamic> str) =>
    List<RejectionHistory>.from(str.map((x) => RejectionHistory.fromJson(x)));

String rejectionHistoryToJson(List<RejectionHistory> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RejectionHistory {
  RejectionHistory({
    this.id,
    this.stateCode,
    this.quntity,
    this.variationCode,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.createdByName,
    this.updatedBy,
    this.updatedByName,
    this.variationData,
  });

  int id;
  String stateCode;
  int quntity;
  String variationCode;
  String description;
  String createdAt;
  String updatedAt;
  int createdBy;
  String createdByName;
  int updatedBy;
  String updatedByName;
  VariationData variationData;

  factory RejectionHistory.fromJson(Map<String, dynamic> json) =>
      RejectionHistory(
        id: json["id"],
        stateCode: json["stateCode"],
        quntity: json["quntity"],
        variationCode:
            json["variationCode"] == null ? null : json["variationCode"],
        description: json["description"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
        createdBy: json["createdBy"],
        createdByName: json["createdByName"],
        updatedBy: json["updatedBy"],
        updatedByName: json["updatedByName"],
        variationData: json["variationData"] == null
            ? null
            : VariationData.fromJson(json["variationData"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "stateCode": stateCode,
        "quntity": quntity,
        "variationCode": variationCode == null ? null : variationCode,
        "description": description,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "createdBy": createdBy,
        "createdByName": createdByName,
        "updatedBy": updatedBy,
        "updatedByName": updatedByName,
        "variationData": variationData == null ? null : variationData.toJson(),
      };
}

class VariationData {
  VariationData({
    this.brand,
    this.type,
    this.holeShape,
    this.size,
    this.variationCode,
    this.netWeight,
    this.cutWeight,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
    this.createdByName,
    this.updatedByName,
    this.variationId,
  });

  String brand;
  String type;
  String holeShape;
  String size;
  String variationCode;
  double netWeight;
  double cutWeight;
  String description;
  String createdAt;
  String updatedAt;
  int createdBy;
  int updatedBy;
  String createdByName;
  String updatedByName;
  int variationId;

  factory VariationData.fromJson(Map<String, dynamic> json) => VariationData(
        brand: json["brand"],
        type: json["type"],
        holeShape: json["holeShape"],
        size: json["size"],
        variationCode: json["variationCode"],
        netWeight: json["netWeight"].toDouble(),
        cutWeight: json["cutWeight"].toDouble(),
        description: json["description"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
        createdByName: json["createdByName"],
        updatedByName: json["updatedByName"],
        variationId: json["variationId"],
      );

  Map<String, dynamic> toJson() => {
        "brand": brand,
        "type": type,
        "holeShape": holeShape,
        "size": size,
        "variationCode": variationCode,
        "netWeight": netWeight,
        "cutWeight": cutWeight,
        "description": description,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "createdBy": createdBy,
        "updatedBy": updatedBy,
        "createdByName": createdByName,
        "updatedByName": updatedByName,
        "variationId": variationId,
      };
}
