// To parse this JSON data, do
//
//     final processHistory = processHistoryFromJson(jsonString);

import 'dart:convert';

List<ProcessHistory> processHistoryFromJson(List<dynamic> str) =>
    List<ProcessHistory>.from(str.map((x) => ProcessHistory.fromJson(x)));

String processHistoryToJson(List<ProcessHistory> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProcessHistory {
  ProcessHistory({
    this.id,
    this.stateCode,
    this.quntity,
    this.type,
    this.weight,
    this.variationCode,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.createdByName,
    this.updatedBy,
    this.updatedByName,
    this.variationData,
  });

  int id;
  String stateCode;
  dynamic quntity;
  String type;
  int weight;
  dynamic variationCode;
  String description;
  String createdAt;
  String updatedAt;
  int createdBy;
  String createdByName;
  int updatedBy;
  String updatedByName;
  dynamic variationData;

  factory ProcessHistory.fromJson(Map<String, dynamic> json) => ProcessHistory(
        id: json["id"],
        stateCode: json["stateCode"],
        quntity: json["quntity"],
        type: json["type"],
        weight: json["weight"],
        variationCode: json["variationCode"],
        description: json["description"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
        createdBy: json["createdBy"],
        createdByName: json["createdByName"],
        updatedBy: json["updatedBy"],
        updatedByName: json["updatedByName"],
        variationData: json["variationData"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "stateCode": stateCode,
        "quntity": quntity,
        "type": type,
        "weight": weight,
        "variationCode": variationCode,
        "description": description,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "createdBy": createdBy,
        "createdByName": createdByName,
        "updatedBy": updatedBy,
        "updatedByName": updatedByName,
        "variationData": variationData,
      };
}
