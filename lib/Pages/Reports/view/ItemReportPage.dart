import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/Reports/controller/ReportController.dart';
import 'package:national_tool_app/Pages/Reports/model/ItemSearchList.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class ItemReportPage extends StatelessWidget {
  TextEditingController weight = TextEditingController();
  CommonWidget _commonWidget = CommonWidget();
  ReportController _reportController = Get.find();
  VariationController _variationController = Get.find();

  ValidationMethods _validationMethods = ValidationMethods();
  double width;
  double height;

  @override
  Widget build(BuildContext context) {
    width = Get.width * 0.95;
    height = Get.height;
    return Scaffold(
      body: Column(
        children: [
          buildReportTextField(context),
          Divider(
            thickness: 1.5,
            color: Theme.of(context).primaryColor,
          ),
          buildReportDataTable(),
        ],
      ),
    );
  }

  Container buildReportDataTable() {
    return Container(
      height: height * 0.45,
      child: FutureBuilder<List<ItemSearch>>(
          future: _reportController.getItemSearchList(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.isNotEmpty) {
                return ListView(
                  padding: const EdgeInsets.all(16),
                  children: [
                    PaginatedDataTable(
                      showCheckboxColumn: false,
                      header: Center(
                          child: Text(
                        'Item Report',
                        style: Theme.of(context).textTheme.headline2,
                      )),
                      rowsPerPage:
                          snapshot.data.length < 5 ? snapshot.data.length : 5,
                      columns: [
                        DataColumn(label: Text('Item')),
                        DataColumn(label: Text('In Progress'), numeric: true),
                        DataColumn(label: Text('Rejected')),
                        DataColumn(label: Text('Finished')),
                        DataColumn(label: Text('Scrap')),
                        DataColumn(label: Text('Orders')),
                      ],
                      source: _DataSource(context, snapshot.data),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: Text('No  Transaction Done Today'),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  Container buildReportTextField(BuildContext context) {
    return Container(
      height: height * 0.28,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _commonWidget.buildDateTimeField(
                  context: context, width: width * 0.5, hintText: 'From'),
              _commonWidget.buildDateTimeField(
                  context: context, width: width * 0.5, hintText: 'To'),
            ],
          ),
          FutureBuilder<List<String>>(
              future: _variationController.codes,
              builder: (ctx, response) {
                return _commonWidget.buildDropDownButton(
                    context: context,
                    typeList: response.hasData ? response.data : [],
                    onChange: _reportController.onChangeVariationType,
                    onValidation: _validationMethods.onVariationType,
                    initialValue: null,
                    labelText: 'Variation Code',
                    width: width);
              }),
          _commonWidget.buildSquareElevatedButton(
              context: context,
              onTap: onTap,
              title: 'Submit',
              width: width * 0.45,
              height: height * 0.07),
        ],
      ),
    );
  }

  void onTap() {
    /**
     *
     */
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    this.valueD,
    this.valueE,
    this.valueF,
    this.valueG,
  );

  final String valueA;
  final int valueB;
  final int valueC;
  final int valueD;
  final int valueE;
  final int valueF;
  final List<StateDetail> valueG;

}

class _DataSource extends DataTableSource {
  final BuildContext context;
  List<_Row> _rows;
  List<ItemSearch> ItemSearchList = [];
  int _selectedCount = 0;
  CommonWidget _commonWidget = CommonWidget();

  _DataSource(this.context, this.ItemSearchList) {
    _rows = ItemSearchList.map((e) => _Row(e.variationCode, e.inProgress,
        e.rejected, e.finished, e.srapping, e.order, e.stateDetails)).toList();
  }

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      cells: [
        DataCell(Text(row.valueA == null ? '' : row.valueA)),
        DataCell(
          _commonWidget.buildSquareElevatedButton(
              context: context,
              onTap: () {
                Get.defaultDialog(
                  title: 'In Process Item',
                  content: Container(
                    height: row.valueG.length * 40.0,
                    width: 200,
                    child: ListView.builder(
                      itemCount: row.valueG.length,
                      itemBuilder: (context, index) => ListTile(
                        horizontalTitleGap: 0,
                        contentPadding: EdgeInsets.all(0),
                        leading: Text(row.valueG[index].stateName),
                        trailing: Text(row.valueG[index].inProgress.toString()),
                      ),
                    ),
                  ),
                );
                // Get.dialog(
                //     Scaffold(
                //       body: Center(
                //         child: Container(
                //           height: Get.height*0.3,
                //           child: Text('kuldeep'),
                //         ),
                //       ),
                //     ),
                //     useRootNavigator: true,
                //     useSafeArea: true,barrierDismissible: true);
              },
              title: row.valueB == null ? 0 : '${row.valueB}',
              width: Get.width * 0.17,
              height: Get.height * 0.03,
              textStyle: Theme.of(context).textTheme.button,
              bgColor: Theme.of(context).primaryColor),
        ),
        DataCell(Text(row.valueC == null ? '' : '${row.valueC}')),
        DataCell(Text(row.valueD == null ? '' : '${row.valueD}')),
        DataCell(Text(row.valueE == null ? '' : '${row.valueE}')),
        DataCell(Text(row.valueF == null ? '' : '${row.valueF}')),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
