import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Reports/controller/ReportController.dart';

class ReportBottomTabBar extends StatefulWidget {
  @override
  _ReportBottomTabBarState createState() => _ReportBottomTabBarState();
}

class _ReportBottomTabBarState extends State<ReportBottomTabBar>
    with SingleTickerProviderStateMixin {
  CommonWidget _commonWidget = CommonWidget();
  ReportController _reportController = Get.find();
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController =
        TabController(length: _reportController.tabsIcon.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _commonWidget.customAppBar(
        title: 'Reports',
        context: context,
      ),
      drawer: CustomDrawer('REPORTS'),
      body: TabBarView(
        children: _reportController.pages,
        controller: _tabController,
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: TabBar(
          isScrollable: true,
          tabs: _reportController.tabsIcon,
          controller: _tabController,
        ),
      ),
    );
  }
}
