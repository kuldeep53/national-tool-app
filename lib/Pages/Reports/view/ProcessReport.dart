import 'dart:ui';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/Reports/controller/ReportController.dart';
import 'package:national_tool_app/Pages/Reports/model/ProcessHistory.dart';

class ProcessReport extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  double width;
  double height;
  TextEditingController weight = TextEditingController();

  ReportController _reportController = Get.find();

  DateTime fromDate = DateTime.now();

  @override
  Widget build(BuildContext context) {
    width = Get.width * 0.95;
    height = Get.height;
    return Scaffold(
        body: Column(
      children: [
        _commonWidget.buildReportTextField(
            width: width, context: context, height: height, onTap: onTap),
        Divider(
          thickness: 1.5,
          color: Theme.of(context).primaryColor,
        ),
        buildReportDataTable()
      ],
    ));
  }

  Container buildReportDataTable() {
    return Container(
      height: height * 0.5,
      child: FutureBuilder<List<ProcessHistory>>(
          future: _reportController.getProcessHistory(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.isNotEmpty) {
                return ListView(
                  padding: const EdgeInsets.all(16),
                  children: [
                    PaginatedDataTable(
                      showCheckboxColumn: false,
                      header: Center(
                          child: Text(
                        'One Day Process Report',
                        style: Theme.of(context).textTheme.headline2,
                      )),
                      rowsPerPage:
                          snapshot.data.length < 5 ? snapshot.data.length : 5,
                      columns: [
                        DataColumn(label: Text('State')),
                        DataColumn(label: Text('Quantity'), numeric: true),
                        DataColumn(label: Text('Size')),
                        DataColumn(label: Text('Created')),
                        DataColumn(label: Text('Created By')),
                      ],
                      source: _DataSource(context, snapshot.data),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: Text('No  Transaction Done Today'),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }

  void onTap() {
    /**
     *
     */
  }
}

class _Row {
  _Row(
    this.valueA,
    this.valueB,
    this.valueC,
    this.valueD,
    this.valueE,
  );

  final String valueA;
  final int valueB;
  final String valueC;
  final String valueD;
  final String valueE;

  bool selected = false;
}

class _DataSource extends DataTableSource {
  final BuildContext context;
  List<_Row> _rows;
  List<ProcessHistory> processList = [];
  int _selectedCount = 0;

  _DataSource(this.context, this.processList) {
    _rows = processList
        .map((e) => _Row(e.stateCode, e.quntity, e.variationCode, e.createdAt,
            e.createdByName))
        .toList();
  }

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _rows.length) return null;
    final row = _rows[index];
    return DataRow.byIndex(
      index: index,
      selected: row.selected,
      onSelectChanged: (value) {
        if (row.selected != value) {
          _selectedCount += value ? 1 : -1;
          assert(_selectedCount >= 0);
          row.selected = value;
          notifyListeners();
        }
      },
      cells: [
        DataCell(Text(row.valueA == null ? '' : row.valueA)),
        DataCell(Text(row.valueB == null ? '' : row.valueB.toString())),
        DataCell(Text(row.valueC == null ? '' : row.valueC)),
        DataCell(Text(row.valueD == null ? '' : row.valueD)),
        DataCell(Text(row.valueE == null ? '' : row.valueE)),
      ],
    );
  }

  @override
  int get rowCount => _rows.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;
}
