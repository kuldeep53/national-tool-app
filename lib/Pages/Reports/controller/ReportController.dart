import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Pages/Reports/model/ItemSearchList.dart';
import 'package:national_tool_app/Pages/Reports/model/ItemSearchReqBean.dart';
import 'package:national_tool_app/Pages/Reports/model/ProcessHistory.dart';
import 'package:national_tool_app/Pages/Reports/model/ProcessHistoryReqBean.dart';
import 'package:national_tool_app/Pages/Reports/model/RejectionHistory.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';

class ReportController extends GetxController {
  final userDetails = GetStorage();
  String dropDownVariationValue;

  String dropDownStateValue = '';
  List<String> stateList = [
    'RAW Material',
    'Cutting',
    'Forzing',
    'Hardening',
    'Reparing',
    'Rejection',
    'Polishing',
    'Finshing'
  ];

  void onChangeStateType(value) {
    dropDownStateValue = value;
    update();
  }

  void onChangeVariationType(value) {
    dropDownVariationValue = value;
    update();
  }

  final DateTime currentDate = DateTime.now();

  Future<List<ProcessHistory>> getProcessHistory(
      {DateTime fromDate, DateTime toDate, String state}) async {
    if (fromDate == null) fromDate = DateTime.now();
    if (toDate == null) toDate = DateTime.now();
    ProcessHistoryReqBean reqBean = ProcessHistoryReqBean(
        stateCode: "FROGGING",
        fromDate: fromDate,
        loginUserId: userDetails.read(Finals.USER_ID),
        toDate: toDate,
        token: userDetails.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_PROCESS_HISTORY, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      return processHistoryFromJson(response.data);
    } else {
      return [];
    }
  }


  Future<List<ItemSearch>> getItemSearchList(
      {DateTime fromDate, DateTime toDate, String variationCode}) async {
    if (fromDate == null) fromDate = DateTime.now();
    if (toDate == null) toDate = DateTime.now();
    ItemSearchReqBean reqBean = ItemSearchReqBean(
        variationCode: variationCode,
        fromDate: fromDate,
        loginUserId: userDetails.read(Finals.USER_ID),
        toDate: toDate,
        token: userDetails.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_VARIATION_DETAIL, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      if (response.data == null) return [];
      return itemSearchFromJson(response.data);
    } else {
      return [];
    }
  }

  Future<List<RejectionHistory>> getRejectionHistory(
      {DateTime fromDate, DateTime toDate, String state}) async {
    if (fromDate == null) fromDate = DateTime.now();
    if (toDate == null) toDate = DateTime.now();
    ProcessHistoryReqBean reqBean = ProcessHistoryReqBean(
        stateCode: null,
        fromDate: fromDate,
        loginUserId: userDetails.read(Finals.USER_ID),
        toDate: toDate,
        token: userDetails.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_REJECTION_HISTORY, reqBody: reqBean.toJson());
    if (response != null && response.responseCode == 0) {
      if (response.data == null) return [];
      return rejectionHistoryFromJson(response.data);
    } else {
      return [];
    }
  }

  List<String> fetchAllReportMenu() {
    List<UserMenu> userMenuList =
        userMenuFromJson(userDetails.read(Finals.MENUS));
    UserMenu userMenu = userMenuList.firstWhere(
        (element) => element.menus.menuCode.toUpperCase() == 'REPORTS');
    List<String> resultList =
        userMenu.childMenus.map((e) => e.menuName).toList();
    return resultList;
  }

  List<String> fetchAllReportMenuCodes() {
    List<UserMenu> userMenuList =
        userMenuFromJson(userDetails.read(Finals.MENUS));
    UserMenu userMenu = userMenuList.firstWhere(
        (element) => element.menus.menuCode.toUpperCase() == 'REPORTS');
    List<String> resultList =
        userMenu.childMenus.map((e) => e.menuCode).toList();
    return resultList;
  }

  List<Widget> get pages =>
      fetchAllReportMenuCodes().map((e) => Finals.reportTabs[e]).toList();

  List<Tab> get tabsIcon => fetchAllReportMenuCodes()
      .map((e) => Tab(
            icon: Icon(Finals.reportIconMap[e]),
            text: e,
          ))
      .toList();
}
