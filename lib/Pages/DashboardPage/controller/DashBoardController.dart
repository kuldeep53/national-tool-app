import 'dart:convert';
import 'dart:io';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/DashboardPage/model/CurrentState.dart';
import 'package:national_tool_app/Pages/DashboardPage/model/StateDetailReqBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';

class DashboardController extends GetxController with StateMixin<CurrentState> {
  final userDetail = GetStorage();

  String title = 'DASHBOARD';

  @override
  void onInit() {
    super.onInit();
    printInfo(info: 'dashboard intialized');
    getCurrentStateData()
        .then((response) => change(response, status: RxStatus.success()),
            onError: (err) {
      change(null, status: RxStatus.error(err.toString()));
    });
  }

  Future<CurrentState> getCurrentStateData() async {
    StateDetailReqBean reqBean = StateDetailReqBean(
        loginUserId: userDetail.read(Finals.USER_ID).toString(),
        token: userDetail.read(Finals.TOKEN));
    printInfo(info: 'getCurrentStateData');
    try {
      ResponseBean response = await Apis.callPostService(
          url: Apis.GET_STATE_DETAIL,
          reqBody: reqBean.toJson(),
          isSuccessBarRequired: false);
      if (response != null && response.responseCode == 0) {
        // printInfo(info: response.data);
        return CurrentState.fromJson(response.data);
      }
      return Future.error('Something went Wrong');
    } catch (e) {
      printError(info: '${e.toString()}');
    }
    return Future.error('data Not loaded Yet');;
  }
}
