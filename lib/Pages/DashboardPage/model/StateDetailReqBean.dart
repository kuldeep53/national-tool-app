
import 'package:flutter/cupertino.dart';

class StateDetailReqBean{
  StateDetailReqBean({
    this.device = 'ANDROID',
    @required this.loginUserId,
    @required this.token,
  });

  String device;
  String loginUserId;
  String token;

  Map<String, dynamic> toJson() => {
    "device": device,
    "loginUserId": loginUserId,
    "token": token,
  };
}