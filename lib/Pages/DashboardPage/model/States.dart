
import 'package:national_tool_app/Pages/DashboardPage/model/StateDetail.dart';

class States {
  States({
    this.stateName,
    this.weight,
    this.quantity,
    this.stateDetails,
  });

  String stateName;
  double weight;
  int quantity;
  List<StateDetail> stateDetails;

  factory States.fromJson(Map<String, dynamic> json) => States(
    stateName: json["stateName"],
    weight: double.parse(json["weight"].toString()),
    quantity: json["quantity"] == null ? null : json["quantity"],
    stateDetails: List<StateDetail>.from(json["stateDetails"].map((x) => StateDetail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "stateName": stateName,
    "weight": weight,
    "quantity": quantity == null ? null : quantity,
    "stateDetails": List<dynamic>.from(stateDetails.map((x) => x.toJson())),
  };
}