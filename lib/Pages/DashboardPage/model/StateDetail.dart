class StateDetail {
  StateDetail({
    this.type,
    this.weight,
    this.quantity,
  });

  String type;
  double weight;
  int quantity;

  factory StateDetail.fromJson(Map<String, dynamic> json) => StateDetail(
    type: json["type"] == null ? null : json["type"],
    weight: json["weight"].toDouble(),
    quantity: json["quantity"] == null ? null : json["quantity"],
  );

  Map<String, dynamic> toJson() => {
    "type": type == null ? null : type,
    "weight": weight,
    "quantity": quantity == null ? null : quantity,
  };
}