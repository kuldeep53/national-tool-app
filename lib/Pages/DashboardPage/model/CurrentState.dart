// To parse this JSON data, do
//
//     final currentState = currentStateFromJson(jsonString);

import 'dart:convert';

import 'package:national_tool_app/Pages/DashboardPage/model/Percentile.dart';
import 'package:national_tool_app/Pages/DashboardPage/model/States.dart';

CurrentState currentStateFromJson(String str) {
  // print(str);
  return CurrentState.fromJson(json.decode(str));
}

String currentStateToJson(CurrentState data) => json.encode(data.toJson());

class CurrentState {
  CurrentState({
    this.percentile,
    this.statesDetail,
  });

  List<Percentile> percentile;
  List<States> statesDetail;

  factory CurrentState.fromJson(Map<String, dynamic> json) {
    // print(json);
    return CurrentState(
      percentile: List<Percentile>.from(
          json["percentile"].map((x) => Percentile.fromJson(x))),
      statesDetail: List<States>.from(
          json["statesDetail"].map((x) => States.fromJson(x))),
    );
  }

  Map<String, dynamic> toJson() => {
        "percentile": List<dynamic>.from(percentile.map((x) => x.toJson())),
        "statesDetail": List<dynamic>.from(statesDetail.map((x) => x.toJson())),
      };
}
