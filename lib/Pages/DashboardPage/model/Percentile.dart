class Percentile {
  Percentile({
    this.stateName,
    this.percentage,
  });

  String stateName;
  String percentage;

  factory Percentile.fromJson(Map<String, dynamic> json) => Percentile(
    stateName: json["stateName"],
    percentage: json["percentage"],
  );

  Map<String, dynamic> toJson() => {
    "stateName": stateName,
    "percentage": percentage,
  };
}