import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/DashboardPage/controller/DashBoardController.dart';
import 'package:national_tool_app/Pages/DashboardPage/model/StateDetail.dart';
import 'package:national_tool_app/Pages/DashboardPage/model/States.dart';
import 'package:national_tool_app/Pages/DashboardPage/view/GraphExample.dart';
import 'package:national_tool_app/Utility/Finals.dart';

import '../../DrawerPage/view/Drawer.dart';

class DashBoard extends GetView<DashboardController> {
  CommonWidget _commonWidget = CommonWidget();
  double _height;

  @override
  Widget build(BuildContext context) {
    double _height = MediaQuery.of(context).size.height * 2;
    return Scaffold(
      appBar: _commonWidget.customAppBar(title: 'DashBoard', context: context),
      drawer: CustomDrawer('DASHBOARD'),
      body: SingleChildScrollView(
        child: Container(
          height: _height,
          child: controller.obx(
            (state) => CustomScrollView(
              slivers: [
                SliverToBoxAdapter(
                  child: SizedBox(
                      height: 400,
                      child: PieChartExample(data: state.percentile)),
                ),
                SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (ctx, index) =>
                        buildExpansionTile(context, state.statesDetail[index]),
                    childCount: state.statesDetail.length,
                  ),
                ),
                SliverFillRemaining(
                  child: Center(
                      child: Text(
                    'SCROLL UP',
                    style: Theme.of(context).textTheme.headline2,
                  )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

// List<PieChartSectionData> showingSections(List<Percentile> percentageData) {
//   return List.generate(percentageData.length, (i) {
//     return PieChartSectionData(
//       color: Finals.graphColors[percentageData[i].stateName],
//       value: double.parse(percentageData[i].percentage.replaceAll('%', '')),
//       title: '${percentageData[i].percentage}%',
//       radius: 140,
//       titlePositionPercentageOffset: 1.15,
//       titleStyle: TextStyle(
//           fontSize: 18,
//           fontWeight: FontWeight.bold,
//           color: const Color(0xff775f34)),
//     );
//   });
// }
//
  ExpansionTile buildExpansionTile(BuildContext context, States stateData) {
    return ExpansionTile(
      title: Text(
        stateData.stateName,
        style: Theme.of(context).textTheme.headline3,
      ),
      subtitle: Text(Finals.stateDescMap[stateData.stateName]),
      trailing: Text(
        stateData.quantity != null
            ? '${stateData.quantity} Nos.'
            : '${stateData.weight} Kgs',
        style: Theme.of(context).textTheme.headline3,
      ),
      children: buildSubItemList(stateData.stateDetails, context),
      expandedAlignment: Alignment.centerLeft,
      leading: ImageIcon(
        AssetImage(Finals.stateIconMap[stateData.stateName]),
        size: 30,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

// Text(
// item,
// style: Theme.of(context).textTheme.headline2,
// );

  List<Widget> buildSubItemList(
      List<StateDetail> subItems, BuildContext context) {
    return subItems.map((subItem) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Column(
          children: [
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              subItem.type != null||subItem.quantity != null
                  ? Text(
                      subItem.type!=null?'${subItem.type} Type of ':'${subItem.quantity} Nos. of ',
                      style: Theme.of(context).textTheme.headline2,
                    )
                  : SizedBox(),
              // subItem.quantity != null
              //     ? Text(
              //         '${subItem.quantity}',
              //         style: Theme.of(context).textTheme.headline2,
              //       )
              //     : SizedBox(),
              subItem.weight != null
                  ? Text(
                      '${subItem.weight} Kgs',
                      style: Theme.of(context).textTheme.headline2,
                    )
                  : SizedBox(),
            ]),
            Divider(
              color: Theme.of(context).primaryColor.withOpacity(0.5),
              thickness: 1,
            ),
          ],
        ),
      );
    }).toList();
  }
}
