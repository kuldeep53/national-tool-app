import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:national_tool_app/Pages/DashboardPage/model/Percentile.dart';
import 'package:national_tool_app/Utility/Finals.dart';

class PieChartExample extends StatefulWidget {
  final List<Percentile> data;

  const PieChartExample({Key key, this.data}) : super(key: key);

  @override
  _PieChartExampleState createState() => _PieChartExampleState();
}

class _PieChartExampleState extends State<PieChartExample> {
  // Chart configs.

  charts.BehaviorPosition _legendPosition = charts.BehaviorPosition.bottom;

  // Data to render.

  @override
  Widget build(BuildContext context) {
    final List<Percentile> data = widget.data;
    final _colorPalettes =
        charts.MaterialPalette.getOrderedPalettes(data.length);
    return ListView(
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        SizedBox(
          height: 400,
          child: charts.PieChart(
            // Pie chart can only render one series.
            /*seriesList=*/
            [
              charts.Series<Percentile, String>(
                id: 'NT-Inventory-1',
                colorFn: (Percentile sales, _) =>
                    charts.ColorUtil.fromDartColor(
                        Finals.graphColors[sales.stateName]),
                domainFn: (Percentile sales, _) =>
                    sales.stateName.toUpperCase(),
                outsideLabelStyleAccessorFn: (datum, index) =>
                    charts.TextStyleSpec(
                        color:
                            charts.ColorUtil.fromDartColor(Color(0xfffffff0))),
                measureFn: (Percentile sales, _) =>
                    double.parse(sales.percentage.replaceAll('%', '')),
                data: data,
                // Set a label accessor to control the text of the arc label.
                labelAccessorFn: (Percentile row, _) =>
                    '${row.stateName}\n${row.percentage}',
              ),
            ],
            animate: true,
            defaultRenderer: charts.ArcRendererConfig(
              arcRendererDecorators: [
                charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.auto)
              ],
            ),
            behaviors: [
              // Add title.
              charts.ChartTitle('Graph Of Current State',
                  behaviorPosition: charts.BehaviorPosition.top,
                  innerPadding: 20),
              // Add legend. ("Datum" means the "X-axis" of each data point.)
              charts.DatumLegend(
                  position: charts.BehaviorPosition.bottom,
                  desiredMaxRows: 2,
                  desiredMaxColumns: 2,
                  outsideJustification:
                      charts.OutsideJustification.middleDrawArea),
            ],
          ),
        ),
        const Divider(),
        // ..._controlWidgets(),
      ],
    );
  }
}
