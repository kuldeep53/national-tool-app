class Menus {
  Menus({
    this.menuCode,
    this.menuName,
    this.relativePath,
    this.absulutePath,
    this.description,
    this.moduleName,
    this.status,
    this.sequence,
  });

  String menuCode;
  String menuName;
  dynamic relativePath;
  dynamic absulutePath;
  String description;
  String moduleName;
  String status;
  int sequence;

  factory Menus.fromJson(Map<String, dynamic> json) => Menus(
    menuCode: json["menuCode"],
    menuName: json["menuName"],
    relativePath: json["relativePath"],
    absulutePath: json["absulutePath"],
    description: json["description"],
    moduleName: json["moduleName"],
    status: json["status"],
    sequence: json["sequence"],
  );

  Map<String, dynamic> toJson() => {
    "menuCode": menuCode,
    "menuName": menuName,
    "relativePath": relativePath,
    "absulutePath": absulutePath,
    "description": description,
    "moduleName": moduleName,
    "status": status,
    "sequence": sequence,
  };
}
