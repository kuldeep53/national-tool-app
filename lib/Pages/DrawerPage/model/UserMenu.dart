// To parse this JSON data, do
//
//     final userMenu = userMenuFromJson(jsonString);

import 'dart:convert';

import 'package:national_tool_app/Pages/DrawerPage/model/Menus.dart';

List<UserMenu> userMenuFromJson(List<dynamic> list) =>
    List<UserMenu>.from(list.map((x) => UserMenu.fromJson(x)));

String userMenuToJson(List<UserMenu> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserMenu {
  UserMenu({
    this.menus,
    this.childMenus,
  });

  Menus menus;
  List<Menus> childMenus;

  factory UserMenu.fromJson(Map<String, dynamic> json) => UserMenu(
        menus: Menus.fromJson(json["menus"]),
        childMenus:
            List<Menus>.from(json["childMenus"].map((x) => Menus.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "menus": menus.toJson(),
        "childMenus": List<dynamic>.from(childMenus.map((x) => x.toJson())),
      };
}
