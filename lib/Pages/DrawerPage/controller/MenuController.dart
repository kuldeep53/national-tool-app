import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenuReqBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';

class MenuController extends GetxController {


  Future<List<dynamic>> getUserMenus(String token, int userId) async {
    UserMenuReqBean reqBean =
        UserMenuReqBean(loginUserId: userId.toString(), token: token);
    printInfo(info: 'UserMenuReq: ${reqBean.toJson()}');
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_USER_MENU,
        reqBody: reqBean.toJson(),
        isSuccessBarRequired: false);
    if (response != null && response.responseCode == 0) {
      return response.data;
    } else
      return null;
  }
}
