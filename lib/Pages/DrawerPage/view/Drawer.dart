import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/view/CustomExpansionTile.dart'
    as custom;
import 'package:national_tool_app/Pages/DrawerPage/model/Menus.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class CustomDrawer extends StatelessWidget {
  Size _size;

  RxString selectedMenuId = RxString('');

  final String MenuCode;

  final userDetail = GetStorage();
  List<UserMenu> userMenuList = [];

  CustomDrawer(this.MenuCode);

  @override
  Widget build(BuildContext context) {
    selectedMenuId.value = MenuCode;
    userMenuList = userMenuFromJson(userDetail.read(Finals.MENUS));
    Menus menus = Menus(
        menuCode: 'LOGOUT',
        relativePath: '/login',
        sequence: 5,
        menuName: 'Logout');
    UserMenu userMenu = UserMenu(menus: menus, childMenus: []);
    userMenuList.add(userMenu);
    _size = Get.size;
    return Drawer(
      elevation: 2,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: _size.height * 0.17,
            child: DrawerHeader(
              decoration: BoxDecoration(color: Theme.of(context).primaryColor),
              margin: EdgeInsets.only(bottom: 0),
              child: buildHeaderContainer(context),
            ),
          ),
          Container(
            height: _size.height * 0.7,
            child: ListView.builder(
              itemBuilder: (context, index) {
                return buildMenuListTile(context, userMenuList[index].menus);
              },
              itemCount: userMenuList.length,
            ),
          )
        ],
      ),
    );
  }

  List<Widget> buildSubMenuTile(BuildContext context, String menuCode) {
    List<Menus> childMenus = userMenuList
        .firstWhere((element) => element.menus.menuCode == menuCode)
        .childMenus
        .map((e) {
          if (e.sequence != 0) return e;
        })
        .where((element) => element != null)
        .toList();
    childMenus.add(userMenuList
        .firstWhere((element) => element.menus.menuCode == menuCode)
        .menus);
    List<Widget> widgetList = childMenus.map((menu) {
      return ListTile(
          selectedTileColor: Theme.of(context).accentColor.withOpacity(0.3),
          title: Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Icon(
                Finals.menuIconMap[menu.menuCode],
                size: 24,
                color: Theme.of(context).primaryColor,
              ),
              SizedBox(
                width: 30,
              ),
              Text(
                menu.menuName,
                style: menu.menuCode == selectedMenuId.value
                    ? Theme.of(context).textTheme.headline4
                    : Theme.of(context).textTheme.headline3,
              ),
            ],
          ),
          selected: menu.menuCode == selectedMenuId.value,
          hoverColor: Theme.of(context).primaryColor,
          onTap: () {

              Get.offNamed(menu.relativePath);
          });
    }).toList();
    return widgetList;
  }

  Widget buildMenuListTile(BuildContext context, Menus menu) {
    return Obx(
      () => menu.menuCode == 'INVENTORY'
          ? custom.ExpansionTile(
              initiallyExpanded: true,
              children: buildSubMenuTile(context, menu.menuCode),
              leading: Icon(
                Finals.menuIconMap[menu.menuCode],
                size: 24,
                color: Theme.of(context).primaryColor,
              ),
              title: Text(
                menu.menuName,
                style: menu.menuCode == selectedMenuId.value
                    ? Theme.of(context).textTheme.headline4
                    : Theme.of(context).textTheme.headline3,
              ),
            )
          : ListTile(
              selectedTileColor: Theme.of(context).accentColor.withOpacity(0.3),
              leading: Icon(
                Finals.menuIconMap[menu.menuCode],
                size: 24,
                color: Theme.of(context).primaryColor,
              ),
              selected: menu.menuCode == selectedMenuId.value,
              hoverColor: Theme.of(context).primaryColor,
              title: Text(
                menu.menuName,
                style: menu.menuCode == selectedMenuId.value
                    ? Theme.of(context).textTheme.headline4
                    : Theme.of(context).textTheme.headline3,
              ),
              onTap: () {
                Get.offNamed(menu.relativePath);
              }),
    );
  }

  Container buildHeaderContainer(BuildContext context) {
    String name = userDetail.read(Finals.NAME);
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 30,
                child: Text(
                  name.capitalizeFirst.substring(0, 1),
                  style: Theme.of(context).textTheme.headline1,
                ),
                backgroundColor: Theme.of(context).canvasColor,
              ),
              SizedBox(
                width: 20,
              ),
              Expanded(
                child: Text(
                  userDetail.read(Finals.NAME),
                  style: Theme.of(context).textTheme.headline4,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
