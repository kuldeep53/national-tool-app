import 'dart:io';

import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:national_tool_app/Pages/Common/controller/SupplierController.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Utility/CounterTextField.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class RawMaterialPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  double width;
  double height;
  final description = TextEditingController();
  final weight = TextEditingController();

  InventoryTabController _inventoryTabController = Get.find();
  SupplierController _supplierController = Get.find();
  var formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    width = Get.width * 0.95;
    height = Get.height;
    return Scaffold(
      body: Center(
        child: Container(
          height: height * 0.6,
          child: Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Fill  In The Raw Material Details',
                  style: Theme.of(context).textTheme.headline2,
                ),
                FutureBuilder<Map<String, int>>(
                    future: _supplierController.supplierNameIdMap,
                    builder: (context, response) {
                      return _commonWidget.buildDropDownButton(
                          context: context,
                          typeList: response.hasData
                              ? response.data.keys.toList()
                              : [],
                          onChange:
                              _inventoryTabController.onChangeSupplierType,
                          onValidation: _validationMethods.onSupplierName,
                          initialValue: null,
                          labelText: 'Supplier Name',
                          width: width);
                    }),
                GetBuilder<InventoryTabController>(builder: (controller) {
                  return _commonWidget.buildDropDownButton(
                      context: context,
                      typeList: controller.typeList,
                      onChange: controller.onChangeRawMaterialType,
                      onValidation: _validationMethods.onMaterialType,
                      initialValue: null,
                      labelText: 'Material Type',
                      width: width);
                }),
                _commonWidget.buildRectTextArea(
                    context: context,
                    hintText: 'DESCRIPTION',
                    textAlign: TextAlign.start,
                    width: width,
                    controller: description),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _commonWidget.buildRectTextField(
                        context: context,
                        hintText: 'WEIGHT',
                        textAlign: TextAlign.start,
                        onValidation: _validationMethods.onWeight,
                        width: width * 0.5,
                        inputType:
                            TextInputType.numberWithOptions(decimal: true),
                        controller: weight),
                    SizedBox(
                      width: width * 0.045,
                    ),
                    _commonWidget.buildSquareElevatedButton(
                        context: context,
                        onTap: onTap,
                        title: 'Submit',
                        width: width * 0.45,
                        height: height * 0.07),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTap() {
    if (formKey.currentState.validate()) {
      _inventoryTabController
          .addRawMaterial(double.parse(weight.text), description.text)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          weight.clear();
          description.clear();
        }
      });
    }
  }
}
