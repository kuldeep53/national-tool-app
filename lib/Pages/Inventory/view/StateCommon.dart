import 'dart:io';

import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/SupplierController.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Pages/Inventory/model/StateEnum.dart';
import 'package:national_tool_app/Utility/CounterTextField.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class StateCommonPage extends StatelessWidget {
  InventoryTabController _inventoryTabController = Get.find();
  VariationController _variationController = Get.find();

  CommonWidget _commonWidget = CommonWidget();
  ValidationMethods _validationMethods = ValidationMethods();
  final description = TextEditingController();
  final quantity = TextEditingController();
  GlobalKey<FormState> formKey=GlobalKey<FormState>();

  final States titleText;
  double width;
  double height;

  StateCommonPage({this.titleText});

  @override
  Widget build(BuildContext context) {
    width = Get.width * 0.95;
    height = Get.height;

    return Scaffold(
      body: Center(
        child: Container(
          height: height * 0.6,
          child: Form(
            key:formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  'Fill Details of ${titleText.toString().split('.').last.capitalizeFirst} '
                  'Process',
                  style: Theme.of(context).textTheme.headline2,
                ),

                FutureBuilder<List<String>>(
                    future: _variationController.codes,
                    builder: (ctx, response) {
                      return _commonWidget.buildDropDownButton(
                          context: context,
                          typeList: response.hasData ? response.data : [],
                          onChange:
                              _inventoryTabController.onChangeVariationType,
                          onValidation: _validationMethods.onVariationType,
                          initialValue: null,
                          labelText: 'Variation Code',
                          width: width);
                    }),
                GetBuilder<InventoryTabController>(builder: (controller) {
                  return _commonWidget.buildDropDownButton(
                      context: context,
                      typeList: controller.sourceType,
                      onChange: controller.onChangeSourceType,
                      onValidation: _validationMethods.onSourceType,
                      initialValue: null,
                      labelText: 'Source',
                      width: width);
                }),
                _commonWidget.buildRectTextArea(
                    context: context,
                    hintText: 'DESCRIPTION',
                    textAlign: TextAlign.start,
                    width: width,
                    controller: description),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _commonWidget.buildRectTextField(
                        context: context,
                        hintText: 'QUANTITY',
                        textAlign: TextAlign.start,
                        onValidation: _validationMethods.onWeight,
                        width: width * 0.5,
                        inputType:
                            TextInputType.numberWithOptions(decimal: true),
                        controller: quantity),
                    _commonWidget.buildSquareElevatedButton(
                        context: context,
                        onTap: onTap,
                        title: 'Submit',
                        width: width * 0.45,
                        height: height * 0.07),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onTap() {
    formKey=formKey==null?GlobalKey<FormState>():formKey;
    if (formKey.currentState.validate()) {
      _inventoryTabController
          .addOtherStateData(
          description.text, int.parse(quantity.text), titleText)
          .then((value) {
        if (value) {
          sleep(Duration(seconds: 1));
          description.clear();
          quantity.clear();
        }
      });
    }
  }
}
