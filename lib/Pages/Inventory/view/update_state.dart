import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Pages/Inventory/model/StateEnum.dart';
import 'package:national_tool_app/Utility/CommonMethods.dart';
import 'package:national_tool_app/Utility/ValidationMethods.dart';

class UpdateState extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  CommonMethods _commonMethods = CommonMethods();
  InventoryTabController _inventoryTabController = Get.find();
  ValidationMethods _validationMethods = ValidationMethods();
  VariationController _variationController = Get.find();
  final description = TextEditingController();
  final quantity = TextEditingController();
  double width;
  double height;
  var getFormKey = GlobalKey<FormState>();
  var postFormKey = GlobalKey<FormState>();

  RxString variationCodeIntialValue = RxString(null);
  RxString sourceTypeIntialValue = RxString(null);
  RxString stateNameIntialValue = RxString(null);
  RxBool postFormFlag = RxBool(false);

  @override
  Widget build(BuildContext context) {
    width = Get.width * 0.95;
    height = Get.height;
    return Scaffold(
      appBar: _commonWidget.customAppBar(title: 'Inventory', context: context),
      drawer: CustomDrawer('UPDATE_STATE_DATA'),
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                height: height,
                child: Obx(
                  () => Column(
                    mainAxisAlignment: postFormFlag.isTrue
                        ? MainAxisAlignment.spaceAround
                        : MainAxisAlignment.start,
                    children: [
                      buildGetForm(context),
                      Divider(
                        thickness: 1.5,
                        color: Theme.of(context).primaryColor,
                      ),
                      postFormFlag.isTrue
                          ? Text(
                              'Update Below State Data',
                              style: Theme.of(context).textTheme.headline2,
                            )
                          : SizedBox(),
                      buildPostForm(context),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPostForm(BuildContext context) {
    return postFormFlag.isFalse
        ? Text(
            'Please Select Variation Code and State',
            style: Theme.of(context).textTheme.headline3,
          )
        : Form(
            key: postFormKey,
            child: Container(
              height: height * 0.6,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  FutureBuilder<List<String>>(
                      future: _variationController.codes,
                      builder: (ctx, response) {
                        return _commonWidget.buildDropDownButton(
                            context: context,
                            typeList: response.hasData ? response.data : [],
                            onChange:
                                _inventoryTabController.onChangeVariationType,
                            onValidation: _validationMethods.onVariationType,
                            initialValue: variationCodeIntialValue.value,
                            labelText: 'Variation Code',
                            width: width);
                      }),
                  _commonWidget.buildRectTextArea(
                      context: context,
                      hintText: 'DESCRIPTION',
                      textAlign: TextAlign.start,
                      width: width,
                      controller: description),
                  GetBuilder<InventoryTabController>(builder: (controller) {
                    return _commonWidget.buildDropDownButton(
                        context: context,
                        typeList: controller.sourceType,
                        onChange: controller.onChangeSourceType,
                        onValidation: _validationMethods.onSourceType,
                        initialValue: sourceTypeIntialValue.value,
                        labelText: 'Source',
                        width: width);
                  }),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      _commonWidget.buildRectTextField(
                          context: context,
                          hintText: 'QUANTITY',
                          textAlign: TextAlign.start,
                          onValidation: _validationMethods.onWeight,
                          width: width * 0.5,
                          inputType:
                              TextInputType.numberWithOptions(decimal: true),
                          controller: quantity),
                      GetBuilder<InventoryTabController>(builder: (controller) {
                        return _commonWidget.buildDropDownButton(
                            context: context,
                            typeList: controller.stateList,
                            onChange: controller.onChangeStateType,
                            onValidation: _validationMethods.onStateName,
                            initialValue: stateNameIntialValue.value,
                            labelText: 'State Name',
                            width: width * 0.5);
                      }),
                    ],
                  ),
                  _commonWidget.buildSquareElevatedButton(
                      context: context,
                      onTap: onTapPost,
                      title: 'Submit',
                      width: width * 0.45,
                      height: height * 0.07),
                ],
              ),
            ),
          );
  }

  Form buildGetForm(BuildContext context) {
    return Form(
      key: getFormKey,
      child: Container(
        height: height * 0.2,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            FutureBuilder<List<String>>(
                future: _variationController.codes,
                builder: (ctx, response) {
                  return _commonWidget.buildDropDownButton(
                      context: context,
                      typeList: response.hasData ? response.data : [],
                      onChange: _inventoryTabController.onChangeVariationType,
                      onValidation: _validationMethods.onVariationType,
                      initialValue: null,
                      labelText: 'Variation Code',
                      width: width);
                }),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GetBuilder<InventoryTabController>(builder: (controller) {
                  return _commonWidget.buildDropDownButton(
                      context: context,
                      typeList: controller.stateList,
                      onChange: controller.onChangeStateType,
                      onValidation: _validationMethods.onStateName,
                      initialValue: null,
                      labelText: 'State Name',
                      width: width * 0.5);
                }),
                _commonWidget.buildSquareElevatedButton(
                    context: context,
                    onTap: onTapGet,
                    title: 'Submit',
                    width: width * 0.45,
                    height: height * 0.07),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void onTapGet() {
    if (getFormKey.currentState.validate())
      _inventoryTabController.getStateData().then((value) {
        if (value != null) {
          sleep(Duration(seconds: 1));
          variationCodeIntialValue.value = value.variationCode;
          sourceTypeIntialValue.value = 'INTERNAL';
          description.text = value.description;
          stateNameIntialValue.value =
              _inventoryTabController.dropDownStateValue;
          quantity.text = value.quntity.toString();
          postFormFlag.value = true;
        }
      });
  }

  void onTapPost() {
    States stateName;
    if (getFormKey.currentState.validate())
      stateName = _commonMethods.enumValueFromString(
          _inventoryTabController.dropDownStateValue.toUpperCase(),
          States.values);
    _inventoryTabController
        .addOtherStateData(
            description.text, int.parse(quantity.text), stateName)
        .then((value) {
      if (value) {
        sleep(Duration(seconds: 1));
        description.clear();
        quantity.clear();
      }
    });
  }
}
