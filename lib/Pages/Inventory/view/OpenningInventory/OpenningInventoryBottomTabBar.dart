import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Pages/Inventory/controller/OpenningInventoryTabController.dart';

class OpenningInventoryBottomTabBar extends StatefulWidget {
  @override
  _OpenningInventoryBottomTabBarState createState() => _OpenningInventoryBottomTabBarState();
}

class _OpenningInventoryBottomTabBarState extends State<OpenningInventoryBottomTabBar>
    with SingleTickerProviderStateMixin {
  CommonWidget _commonWidget = CommonWidget();
  TabController _tabController;
  OpenningInventoryTabController inventoryTabController = Get.find();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: inventoryTabController.tabsIcon.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _commonWidget.customAppBar(title: 'Openning Inventory', context:
      context),
      drawer: CustomDrawer('OPENING_INVENTORY'),
      body: TabBarView(
        children: inventoryTabController.pages,
        controller: _tabController,
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: TabBar(
          isScrollable: true,
          tabs: inventoryTabController.tabsIcon,
          controller: _tabController,
        ),
      ),
    );
  }
}
