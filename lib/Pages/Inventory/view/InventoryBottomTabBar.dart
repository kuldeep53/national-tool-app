import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';
import 'package:national_tool_app/Pages/DrawerPage/view/Drawer.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';

class InventoryBottomTabBar extends StatefulWidget {
  @override
  _InventoryBottomTabBarState createState() => _InventoryBottomTabBarState();
}

class _InventoryBottomTabBarState extends State<InventoryBottomTabBar>
    with SingleTickerProviderStateMixin {
  CommonWidget _commonWidget = CommonWidget();
  TabController _tabController;
  InventoryTabController inventoryTabController = Get.find();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: inventoryTabController.tabsIcon.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _commonWidget.customAppBar(title: 'Inventory', context: context),
      drawer: CustomDrawer('INVENTORY'),
      body: TabBarView(
        children: inventoryTabController.pages,
        controller: _tabController,
      ),
      bottomNavigationBar: Material(
        color: Theme.of(context).primaryColor,
        child: TabBar(
          isScrollable: true,
          tabs: inventoryTabController.tabsIcon,
          controller: _tabController,
        ),
      ),
    );
  }
}
