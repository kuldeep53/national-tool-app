import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/controller/SupplierController.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/Common/model/SupplierRespBean.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/Menus.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Pages/Inventory/model/AddRawReqBean.dart';
import 'package:national_tool_app/Pages/Inventory/model/StateEnum.dart';
import 'package:national_tool_app/Pages/Inventory/model/StateReqBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';

class OpenningInventoryTabController extends GetxController {
  final userDetail = GetStorage();

  // SupplierController _supplierController = Get.find();
  // VariationController _variationController = Get.find();
  int tabIndex = 0;
  List<String> typeList = ['ROUND', 'SQUARE', 'MIXED'];
  String dropDownTypeValues = 'ROUND';
  List<String> sourceType = ['INTERNAL', 'EXTERNAL'];
  String dropDownSourceValue;
  List<String> variationCodes;
  String dropDownVariationValue;
  String dropDowmSupplierValues = '';
  Map<String, int> supplierNameIdMap = {};
  String dropDownStateValue = '';
  List<String> stateList = [
    'RAW Material',
    'Cutting',
    'Forzing',
    'Hardening',
    'Reparing',
    'Rejection',
    'Polishing',
    'Finshing'
  ];
  GlobalKey<FormState> formKey;

  GlobalKey<FormState> globalKeyNew(){
    return formKey = new GlobalKey<FormState>();
  }


  void onChangeStateType(value) {
    dropDownStateValue = value;
    update();
  }

  void changeTabIndex(int index) {
    tabIndex = index;
    update();
  }

  List<String> fetchAllStates() {
    List<UserMenu> userMenuList =
        userMenuFromJson(userDetail.read(Finals.MENUS));
    UserMenu userMenu = userMenuList.firstWhere(
        (element) => element.menus.menuCode.toUpperCase() == 'INVENTORY');
    List<String> resultList = userMenu.childMenus
        .map((e) {
          if (e.sequence == 0) return e.menuName;
        })
        .where((element) => element != null)
        .toList();
    return resultList;
  }

  List<Widget> get pages =>
      fetchAllStates().map((e) => Finals.openningInventoryTabs[e]).toList();

  List<Tab> get tabsIcon => fetchAllStates()
      .map((e) => Tab(
            icon: ImageIcon(AssetImage(Finals.stateIconMap[e])),
            text: e,
          ))
      .toList();

  Future<bool> addRawMaterial(double weight, String desc) async {
    AddRawReqBean reqBean = AddRawReqBean(
      loginUserId: userDetail.read(Finals.USER_ID),
      token: userDetail.read(Finals.TOKEN),
      type: dropDownTypeValues,
      weight: weight,
      description: desc,
      suppId: supplierNameIdMap[dropDowmSupplierValues],
    );
    ResponseBean response = await Apis.callPostService(
        url: Apis.ADD_RAW_MATERIAL,
        reqBody: reqBean.toJson(),
        successMessage: 'Raw Material Added SuccesFully');
    if (response != null && response.responseCode == 0) {
      return true;
    } else {
      return false;
    }
  }


  Future<bool> addOtherStateData(
      String desc, int quantity, States state) async {
    StateReqBean reqBean = StateReqBean(
      loginUserId: userDetail.read(Finals.USER_ID),
      token: userDetail.read(Finals.TOKEN),
      isOuterSource: dropDownSourceValue == 'INTERNAL' ? 'NO' : 'YES',
      variationCode: dropDownVariationValue,
      quantity: quantity,
      description: desc,
      opening: "YES",
    );
    ResponseBean response;
    switch (state) {
      case States.CUTTING:
        response = await Apis.callPostService(
            url: Apis.ADD_CUTTING,
            reqBody: reqBean.toJson(),
            successMessage:
            '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;

      case States.HARDENING:
        response = await Apis.callPostService(
            url: Apis.ADD_HARDENING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.FORZING:
        response = await Apis.callPostService(
            url: Apis.ADD_FORZING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.COLORING:
        response = await Apis.callPostService(
            url: Apis.ADD_COLORING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.POLISHING:
        response = await Apis.callPostService(
            url: Apis.ADD_POLISHING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.FINISHING:
        response = await Apis.callPostService(
            url: Apis.ADD_FINISHING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.REPARING:
        response = await Apis.callPostService(
            url: Apis.ADD_REPARING,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
      case States.REJECTION:
        response = await Apis.callPostService(
            url: Apis.ADD_REJECTION,
            reqBody: reqBean.toJson(),
            successMessage:
                '${state.toString().split('.').last.capitalizeFirst} Material Added '
                'Successfully');
        break;
    }
    if (response != null && response.responseCode == 0) {
      return true;
    } else {
      return false;
    }
  }

  void onChangeRawMaterialType(value) {
    dropDownTypeValues = value;
    update();
  }

  void onChangeSupplierType(value) {
    dropDowmSupplierValues = value;
    update();
  }

  void onChangeSourceType(value) {
    dropDownSourceValue = value;
    update();
  }

  void onChangeVariationType(value) {
    dropDownVariationValue = value;
    update();
  }
}
