// To parse this JSON data, do
//
//     final getStateReqBean = getStateReqBeanFromJson(jsonString);

import 'dart:convert';

GetStateReqBean getStateReqBeanFromJson(String str) => GetStateReqBean.fromJson(json.decode(str));

String getStateReqBeanToJson(GetStateReqBean data) => json.encode(data.toJson());

class GetStateReqBean {
  GetStateReqBean({
    this.variationCode,
    this.device="ANDROID",
    this.loginUserId,
    this.token,
  });

  String variationCode;
  String device;
  int loginUserId;
  String token;

  factory GetStateReqBean.fromJson(Map<String, dynamic> json) => GetStateReqBean(
    variationCode: json["variationCode"],
    device: json["device"],
    loginUserId: json["loginUserId"],
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "variationCode": variationCode,
    "device": device,
    "loginUserId": loginUserId,
    "token": token,
  };
}
