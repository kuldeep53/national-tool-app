import 'package:flutter/cupertino.dart';
import 'package:national_tool_app/Utility/Finals.dart';

/// description : "<string>"
/// device : "<string>"
/// loginUserId : 1
/// quantity : 1
/// token : "<string>"
/// type : "<string>"
/// weight : 0.0

class AddRawReqBean {
  String _description;
  String _device;
  int _loginUserId;
  int _quantity;
  String _token;
  String _type;
  double _weight;
  int _suppId;

  String get description => _description;

  String get device => _device;

  int get loginUserId => _loginUserId;

  int get suppId => _suppId;

  int get quantity => _quantity;

  String get token => _token;

  String get type => _type;

  double get weight => _weight;

  AddRawReqBean(
      {String description,
      String device = Finals.DEVICE,
      @required int loginUserId,
      int quantity,
      @required String token,
      @required String type,
      @required double weight,
      int suppId}) {
    _description = description;
    _device = device;
    _loginUserId = loginUserId;
    _quantity = quantity;
    _token = token;
    _type = type;
    _weight = weight;
    _suppId = suppId;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["description"] = _description;
    map["device"] = _device;
    map["loginUserId"] = _loginUserId;
    map["quantity"] = _quantity;
    map["token"] = _token;
    map["type"] = _type;
    map["weight"] = _weight;
    map['suppId'] = _suppId;
    map.removeWhere((key, value) => key == null || value == null);
    return map;
  }
}
