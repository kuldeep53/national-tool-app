import 'package:flutter/cupertino.dart';

/// description : "<string>"
/// device : "<string>"
/// isOuterSource : "<string>"
/// loginUserId : 1
/// quantity : 1
/// token : "<string>"
/// variationCode : "<string>"

class StateReqBean {
  String _description;
  String _device;
  String _isOuterSource;
  int _loginUserId;
  int _quantity;
  String _token;
  String _variationCode;
  String _opening;

  String get description => _description;
  String get device => _device;
  String get isOuterSource => _isOuterSource;
  int get loginUserId => _loginUserId;
  int get quantity => _quantity;
  String get token => _token;
  String get variationCode => _variationCode;
  String get opening => _opening;

  StateReqBean({
      String description, 
      String device='ANDROID',
      String isOuterSource='NO',
      String opening='NO',
      @required int loginUserId,
      @required int quantity,
      @required String token,
      @required String variationCode}){
    _description = description;
    _device = device;
    _isOuterSource = isOuterSource;
    _loginUserId = loginUserId;
    _quantity = quantity;
    _token = token;
    _variationCode = variationCode;
    _opening=opening;
}

  StateReqBean.fromJson(dynamic json) {
    _description = json["description"];
    _device = json["device"];
    _isOuterSource = json["isOuterSource"];
    _loginUserId = json["loginUserId"];
    _quantity = json["quantity"];
    _token = json["token"];
    _variationCode = json["variationCode"];
    _opening=json['opening'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["description"] = _description;
    map["device"] = _device;
    map["isOuterSource"] = _isOuterSource;
    map["loginUserId"] = _loginUserId;
    map["quantity"] = _quantity;
    map["token"] = _token;
    map["variationCode"] = _variationCode;
    map["opening"]=_opening;
    return map;
  }

}