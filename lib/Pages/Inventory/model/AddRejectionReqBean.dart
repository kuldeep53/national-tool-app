// To parse this JSON data, do
//
//     final addRejectionReqBean = addRejectionReqBeanFromJson(jsonString);

import 'dart:convert';

AddRejectionReqBean addRejectionReqBeanFromJson(String str) => AddRejectionReqBean.fromJson(json.decode(str));

String addRejectionReqBeanToJson(AddRejectionReqBean data) => json.encode(data.toJson());

class AddRejectionReqBean {
  AddRejectionReqBean({
    this.description,
    this.device="ANDROID",
    this.loginUserId,
    this.quantity,
    this.stateCode,
    this.token,
    this.variationCode,
    this.weight,
  });

  String description;
  String device;
  int loginUserId;
  int quantity;
  String stateCode;
  String token;
  String variationCode;
  double weight;

  factory AddRejectionReqBean.fromJson(Map<String, dynamic> json) => AddRejectionReqBean(
    description: json["description"],
    device: json["device"],
    loginUserId: json["loginUserId"],
    quantity: json["quantity"],
    stateCode: json["stateCode"],
    token: json["token"],
    variationCode: json["variationCode"],
    weight: json["weight"],
  );

  Map<String, dynamic> toJson() => {
    "description": description,
    "device": device,
    "loginUserId": loginUserId,
    "quantity": quantity,
    "stateCode": stateCode,
    "token": token,
    "variationCode": variationCode,
    "weight": weight,
  };
}
