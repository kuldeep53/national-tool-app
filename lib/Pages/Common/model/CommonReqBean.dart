import 'package:flutter/foundation.dart';

/// device : "<string>"
/// loginUserId : 1
/// token : "<string>"

class CommonReqBean {
  String _device;
  int _loginUserId;
  String _token;

  String get device => _device;
  int get loginUserId => _loginUserId;
  String get token => _token;

  CommonReqBean({
      String device='ANDROID',
      @required int loginUserId,
      @required String token}){
    _device = device;
    _loginUserId = loginUserId;
    _token = token;
}

  CommonReqBean.fromJson(dynamic json) {
    _device = json["device"];
    _loginUserId = json["loginUserId"];
    _token = json["token"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["device"] = _device;
    map["loginUserId"] = _loginUserId;
    map["token"] = _token;
    return map;
  }

}