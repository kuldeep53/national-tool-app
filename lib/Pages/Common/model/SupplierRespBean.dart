import 'dart:convert';

/// suppId : 1
/// name : "Demp"
/// city : "J0DHPUR"
/// mobileNo : "987546215"
/// emailId : "string@csac.sa"
/// description : "add Supplier"
/// createdAt : 1615711794000
/// updatedAt : 1615711794000
/// createdBy : 1
/// updatedBy : 1
List<SupplierRespBean> supplierRespBeanFromJson(List<dynamic> str) =>
    List<SupplierRespBean>.from(str.map((x) => SupplierRespBean.fromJson(x)));

class SupplierRespBean {
  int _suppId;
  String _name;
  String _city;
  String _mobileNo;
  String _emailId;
  String _description;
  DateTime _createdAt;
  DateTime _updatedAt;
  int _createdBy;
  int _updatedBy;

  int get suppId => _suppId;

  String get name => _name;

  String get city => _city;

  String get mobileNo => _mobileNo;

  String get emailId => _emailId;

  String get description => _description;

  DateTime get createdAt => _createdAt;

  DateTime get updatedAt => _updatedAt;

  int get createdBy => _createdBy;

  int get updatedBy => _updatedBy;

  SupplierRespBean(
      {int suppId,
      String name,
      String city,
      String mobileNo,
      String emailId,
      String description,
      DateTime createdAt,
      DateTime updatedAt,
      int createdBy,
      int updatedBy}) {
    _suppId = suppId;
    _name = name;
    _city = city;
    _mobileNo = mobileNo;
    _emailId = emailId;
    _description = description;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _createdBy = createdBy;
    _updatedBy = updatedBy;
  }

  SupplierRespBean.fromJson(dynamic json) {
    _suppId = json["suppId"];
    _name = json["name"];
    _city = json["city"];
    _mobileNo = json["mobileNo"];
    _emailId = json["emailId"];
    _description = json["description"];
    _createdAt = DateTime.fromMicrosecondsSinceEpoch(json["createdAt"]);
    _updatedAt = DateTime.fromMicrosecondsSinceEpoch(json["updatedAt"]);
    _createdBy = json["createdBy"];
    _updatedBy = json["updatedBy"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["suppId"] = _suppId;
    map["name"] = _name;
    map["city"] = _city;
    map["mobileNo"] = _mobileNo;
    map["emailId"] = _emailId;
    map["description"] = _description;
    map["createdAt"] = _createdAt;
    map["updatedAt"] = _updatedAt;
    map["createdBy"] = _createdBy;
    map["updatedBy"] = _updatedBy;
    return map;
  }
}
