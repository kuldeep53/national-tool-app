// To parse this JSON data, do
//
//     final variationCode = variationCodeFromJson(jsonString);

import 'dart:convert';

List<VariationCode> variationCodeFromJson(List<dynamic> str) =>
    List<VariationCode>.from(str.map((x) => VariationCode.fromJson(x)));

String variationCodeToJson(List<VariationCode> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class VariationCode {
  VariationCode({
    this.brand,
    this.type,
    this.holeShape,
    this.size,
    this.variationCode,
    this.netWeight,
    this.cutWeight,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.createdBy,
    this.updatedBy,
    this.createdByName,
    this.updatedByName,
    this.variationId,
  });

  String brand;
  String type;
  String holeShape;
  String size;
  String variationCode;
  double netWeight;
  double cutWeight;
  String description;
  String createdAt;
  String updatedAt;
  int createdBy;
  int updatedBy;
  String createdByName;
  String updatedByName;
  int variationId;

  factory VariationCode.fromJson(Map<String, dynamic> json) => VariationCode(
        brand: json["brand"],
        type: json["type"],
        holeShape: json["holeShape"],
        size: json["size"],
        variationCode: json["variationCode"],
        netWeight:
            json["netWeight"] != null ? json["netWeight"].toDouble() : null,
        cutWeight:
            json["cutWeight"] != null ? json["netWeight"].toDouble() : null,
        description: json["description"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
        createdBy: json["createdBy"],
        updatedBy: json["updatedBy"],
        createdByName: json["createdByName"],
        updatedByName: json["updatedByName"],
      variationId:json['variationId']
      );

  Map<String, dynamic> toJson() => {
        "brand": brand,
        "type": type,
        "holeShape": holeShape,
        "size": size,
        "variationCode": variationCode,
        "netWeight": netWeight,
        "cutWeight": cutWeight,
        "description": description,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
        "createdBy": createdBy,
        "updatedBy": updatedBy,
        "createdByName": createdByName,
        "updatedByName": updatedByName,
    "variationId":variationId
      };
}
