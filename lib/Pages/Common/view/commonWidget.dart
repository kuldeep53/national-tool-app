import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:national_tool_app/Pages/Reports/controller/ReportController.dart';
import 'package:national_tool_app/Utility/NTMessage.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class CommonWidget {
  static final CommonWidget _commonWidget = CommonWidget._internal();

  factory CommonWidget() {
    return _commonWidget;
  }

  CommonWidget._internal();

  //------------widgets---------------

  //Only used for Report field in
  Container buildReportTextField(
      {@required BuildContext context,
      @required double width,
      @required double height,
      @required Function onTap}) {
    return Container(
      height: height * 0.25,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _commonWidget.buildDateTimeField(
                  context: context, width: width * 0.5, hintText: 'From'),
              _commonWidget.buildDateTimeField(
                  context: context, width: width * 0.5, hintText: 'To'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GetBuilder<ReportController>(builder: (controller) {
                return _commonWidget.buildDropDownButton(
                    context: context,
                    typeList: controller.stateList,
                    onChange: controller.onChangeStateType,
                    // onValidation: _validationMethods.onSupplierName,
                    initialValue: null,
                    labelText: 'State Name',
                    width: width * 0.5);
              }),
              _commonWidget.buildSquareElevatedButton(
                  context: context,
                  onTap: onTap,
                  title: 'Submit',
                  width: width * 0.45,
                  height: height * 0.07),
            ],
          )
        ],
      ),
    );
  }

  Container buildDateTimeField(
      {BuildContext context, double width, String hintText}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: width,
      child: DateTimeField(
        format: DateFormat.yMd(),
        decoration: InputDecoration(
          labelText: hintText,
          labelStyle: Theme.of(context).textTheme.headline6,
          hintStyle: TextStyle(fontSize: 14),
          alignLabelWithHint: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.5)),
          // prefixIcon:  Icon(icon),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.5)),
        ),
        onShowPicker: (context, currentValue) {
          return showDatePicker(
              context: context,
              firstDate: DateTime(1900),
              initialDate: currentValue ?? DateTime.now(),
              lastDate: DateTime(2100));
        },
      ),
    );
  }

  Container buildDropDownButton(
      {@required BuildContext context,
      @required List<String> typeList,
      @required Function onChange,
      Function onValidation,
      String initialValue,
      @required String labelText,
      double width = 350,
      String errorText = NTMessage.COMMON_VALIDATION}) {
    //this will only excute either with statefull widget or GetBuilder as i
    // have given below example
    /**
     * GetBuilder<InventoryTabController>(builder: (controller) {
        return _commonWidget.buildDropDownButton(
        context,
        controller.typeList,
        controller.onChangeRawMaterialType,
        controller.dropDownValue);
        }),
     */
    return Container(
      width: width,
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: DropdownButtonFormField<String>(
        decoration: InputDecoration(
          labelText: labelText,
          labelStyle: Theme.of(context).textTheme.headline6,
          alignLabelWithHint: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.5)),
        ),
        validator: (String val) => onValidation(val),
        isExpanded: true,
        value: initialValue == null ? null : initialValue,
        icon: const Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        onChanged: onChange,
        items: typeList.map<DropdownMenuItem<String>>((value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text(value),
          );
        }).toList(),
      ),
    );
  }

  Container buildSquareElevatedButton(
      {@required BuildContext context,
      @required Function onTap,
      String title = 'SUBMIT',
      double width = 150,
      double height = 45,
      TextStyle textStyle,
      Color bgColor}) {
    return Container(
      width: width,
      height: height,
      child: ElevatedButton(
        onPressed: onTap,
        child: FittedBox(
          child: Text(
            title.toUpperCase(),
            style: textStyle == null
                ? Theme.of(context).textTheme.headline5
                : textStyle,
          ),
        ),
        style: ButtonStyle(
            // elevation: 10,
            backgroundColor: MaterialStateProperty.all<Color>(
                bgColor == null ? Theme.of(context).accentColor : bgColor)),
      ),
    );
  }

  Container buildRectTextField(
      {@required BuildContext context,
      String hintText,
      IconData icon,
      @required TextEditingController controller,
      TextAlign textAlign = TextAlign.center,
      double width = 350,
      TextInputType inputType = TextInputType.text,
      Function onValidation,
      Function onTap}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: width,
      child: TextFormField(
        onTap: null,
        validator: (String val) => onValidation(val),
        keyboardType: inputType,
        controller: controller,
        textAlign: textAlign,
        decoration: InputDecoration(
          labelText: hintText,
          labelStyle: Theme.of(context).textTheme.headline6,
          hintStyle: TextStyle(fontSize: 14),
          alignLabelWithHint: true,
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.5)),
          prefixIcon: icon == null
              ? null
              : Icon(
                  icon,
                  color: Theme.of(context).accentColor,
                ),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context).primaryColor, width: 2.5)),
        ),
      ),
    );
  }

  Container buildRectTextArea(
      {@required BuildContext context,
      String hintText,
      IconData icon,
      TextAlign textAlign = TextAlign.center,
      double width = 300,
      TextEditingController controller,
      @required Function onValidation}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: width,
      child: TextFormField(
        controller: controller,
        maxLines: 5,
        textAlign: textAlign,
        validator: (String val) =>
            onValidation(val),
        decoration: InputDecoration(
            labelText: hintText,
            labelStyle: Theme.of(context).textTheme.headline6,
            hintStyle: TextStyle(fontSize: 14),
            alignLabelWithHint: true,
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Theme.of(context).primaryColor, width: 2.5)),
            focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: Theme.of(context).primaryColor, width: 2.5)),
            icon: icon == null ? null : Icon(icon)),
      ),
    );
  }

  Container buildDropDownList(
      {BuildContext context,
      String hintText,
      IconData icon,
      TextAlign textAlign = TextAlign.center,
      List<String> dropDownList,
      Function onChange,
      String value,
      double width = 300}) {
    String _selectedItem;
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        width: width,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          border: Border.all(
            color: Theme.of(context).primaryColor,
            width: 2.5,
          ),
        ),
        child: DropdownButtonFormField(
          value: value,
          onChanged: onChange,
          decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(fontSize: 14),
            alignLabelWithHint: true,
            border: InputBorder.none,
            icon: icon == null ? null : Icon(icon),
          ),
          items: [
            DropdownMenuItem(child: Text('INTERNAL')),
            DropdownMenuItem(child: Text('EXTERNAL'))
          ],
        ));
  }

  AppBar customAppBar(
      {@required String title,
      @required BuildContext context,
      List<Widget> action}) {
    return AppBar(
      backgroundColor: Theme.of(context).canvasColor,
      actions: action,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(
              Icons.short_text,
              size: 40,
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      title: Text(
        title.toUpperCase(),
        style: Theme.of(context).textTheme.headline3,
      ),
      bottom: PreferredSize(
        child: Container(
          color: Theme.of(context).primaryColor,
          height: 3.0,
        ),
        preferredSize: Size.fromHeight(4.0),
      ),
    );
  }

  AppBar customAppBarWithoutDrawer(
      {@required String title,
      @required BuildContext context,
      List<Widget> action}) {
    return AppBar(
      backgroundColor: Theme.of(context).canvasColor,
      title: Text(
        title.toUpperCase(),
        style: Theme.of(context).textTheme.headline3,
      ),
      actions: action,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: const Icon(
              Icons.arrow_back,
              size: 30,
            ),
            onPressed: () {
              Get.offAndToNamed(Routes.CONTROL);
            },
            tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          );
        },
      ),
      bottom: PreferredSize(
        child: Container(
          color: Theme.of(context).primaryColor,
          height: 3.0,
        ),
        preferredSize: Size.fromHeight(4.0),
      ),
    );
  }

  Column boldTextWithDivider(BuildContext context, String text) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Text(
            text,
            style: TextStyle(
              color: Theme.of(context).hoverColor,
              fontSize: 60,
              fontFamily: 'Times New Roman',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          width: 350,
          child: Divider(
            thickness: 5,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ],
    );
  }

  Container roundedTextFieldContainer({
    @required BuildContext context,
    @required Size size,
    @required String hintText,
    String validationMessage = 'Enter Some Text Can\'t be Empty',
    IconData icon = Icons.account_box_rounded,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.8,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(29),
          color: Theme.of(context).buttonColor.withOpacity(0.2)),
      child: TextFormField(
        validator: (value) => value.isEmpty ? validationMessage : null,
        // onChanged: (value) {
        //   print(value);
        // },
        cursorColor: Theme.of(context).hoverColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: Theme.of(context).primaryColor,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }

  InkWell roundedInkWellButton({
    @required BuildContext context,
    String text = 'LOGIN',
    @required Function onTap,
  }) {
    Size size = MediaQuery.of(context).size;
    return InkWell(
      onTap: onTap,
      splashColor: Theme.of(context).hoverColor,
      borderRadius: BorderRadius.circular(30),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        width: size.width * 0.8,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Theme.of(context).primaryColor.withOpacity(0.8),
              Theme.of(context).primaryColor,
            ],
          ),
          border: Border.all(color: Theme.of(context).primaryColor, width: 4),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
              color: Theme.of(context).buttonColor,
              fontSize: 25,
              fontWeight: FontWeight.bold,
              fontFamily: 'Times New Roman',
            ),
          ),
        ),
      ),
    );
  }
}
