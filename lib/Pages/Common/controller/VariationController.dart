import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/model/CommonReqBean.dart';
import 'package:national_tool_app/Pages/Common/model/VariationCode.dart';
import 'package:national_tool_app/Pages/ControlPanel/model/AddVariationReqBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class VariationController extends GetxController {
  final userDetail = GetStorage();

  List<VariationCode> variationCodes = [];
  RxList<VariationCode> filterVariations = RxList([]);
  RxSet<String> brands = RxSet({});
  RxSet<String> types = RxSet({});
  RxSet<String> holeShapes = RxSet({});
  RxSet<String> sizes = RxSet({});
  String dropDownBrand = '';
  String dropDownType = '';
  String dropDownHole = '';
  String dropDownSize = '';

  void onChangeBrand(value) {
    dropDownBrand = value;
    update();
  }

  void onChangeType(value) {
    dropDownType = value;
    update();
  }

  void onChangeHole(value) {
    dropDownHole = value;
    update();
  }

  void onChangeSize(value) {
    dropDownSize = value;
    update();
  }

  void assignBrands(List<VariationCode> codes) {
    brands.assignAll(codes.map((e) => e.brand).toList());
    update();
  }

  void assignType(List<VariationCode> codes) {
    types.assignAll(codes.map((e) => e.type).toList());
    update();
  }

  void assignHoleShape(List<VariationCode> codes) {
    holeShapes.assignAll(codes.map((e) => e.holeShape).toList());
    update();
  }

  void assignSize(List<VariationCode> codes) {
    sizes.assignAll(codes.map((e) => e.size).toList());
    update();
  }

  Future<List<VariationCode>> fetchAllVariationCodes() async {
    CommonReqBean reqBean = CommonReqBean(
        loginUserId: userDetail.read(Finals.USER_ID),
        token: userDetail.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_VARIATION,
        reqBody: reqBean.toJson(),
        isSuccessBarRequired: false);
    if (response != null && response.responseCode == 0) {
      List<VariationCode> codes = variationCodeFromJson(response.data);
      assignBrands(codes);
      assignType(codes);
      assignSize(codes);
      assignHoleShape(codes);
      variationCodes = codes;
      return codes;
    } else
      return [];
  }

  Future<List<String>> get codes async => await fetchAllVariationCodes()
      .then((value) => value.map((e) => e.variationCode).toList());

  void filterVariationData() {
    bool flag = true;
    if (dropDownBrand.isNotEmpty) {
      filterVariations.assignAll(filterVariations
          .where((element) => element.brand == dropDownBrand)
          .toList());
      flag = false;
    }
    if (dropDownHole.isNotEmpty) {
      filterVariations.assignAll(filterVariations
          .where((element) => element.holeShape == dropDownHole)
          .toList());
      flag = false;
    }
    if (dropDownSize.isNotEmpty) {
      filterVariations.assignAll(filterVariations
          .where((element) => element.size == dropDownSize)
          .toList());
      flag = false;
    }
    if (dropDownType.isNotEmpty) {
      filterVariations.assignAll(filterVariations
          .where((element) => element.type == dropDownType)
          .toList());
      flag = false;
    }
    if (flag) {
      filterVariations.assignAll(variationCodes);
      update();
    }
  }

  void getVariationDetail(String code) {
    VariationCode variation =
        variationCodes.firstWhere((element) => element.variationCode == code);
    Get.offAndToNamed(Routes.UPDATE_VARIATIONS, arguments: variation);
  }

  Future<bool> updateVariationData(
      {String brand,
      String type,
      String size,
      String holeShape,
      double cutWeight,
      double netWeight,
      VariationCode variationCode}) async {
    if (!(brand == variationCode.brand &&
        type == variationCode.type &&
        size == variationCode.size &&
        holeShape == variationCode.holeShape &&
        cutWeight == variationCode.cutWeight &&
        netWeight == variationCode.netWeight)) {
      AddVariationReqBean reqBean = AddVariationReqBean(
          variationId: variationCode.variationId,
          loginUserId: userDetail.read(Finals.USER_ID),
          token: userDetail.read(Finals.TOKEN),
          brand: brand!=variationCode.brand?brand:null,
          type: type!=variationCode.type?type:null,
          size: size!=variationCode.size?size:null,
          holeShape: holeShape!=variationCode.holeShape?holeShape:null,
          cutWeight: cutWeight!=variationCode.cutWeight?cutWeight:null,
          netWeight: netWeight!=variationCode.netWeight?netWeight:null,
          );
      ResponseBean response = await Apis.callPostService(
          url: Apis.UPDATE_VARIATION, reqBody: reqBean.toJson());
      if (response != null && response.responseCode == 0)
        return true;
      else
        return false;
    }
    return true;
  }


  Future<bool> addVariationData(
      {String brand,
        String type,
        String size,
        String holeShape,
        double cutWeight,
        double netWeight,String description}) async {

      AddVariationReqBean reqBean = AddVariationReqBean(
        loginUserId: userDetail.read(Finals.USER_ID),
        token: userDetail.read(Finals.TOKEN),
        brand: brand,
        type: type,
        size: size,
        holeShape: holeShape,
        cutWeight: cutWeight,
        netWeight: netWeight,
        description: description,
      );
      ResponseBean response = await Apis.callPostService(
          url: Apis.ADD_VARIATION, reqBody: reqBean.toJson());
      if (response != null && response.responseCode == 0)
        return true;
      else
        return false;
    }

}
