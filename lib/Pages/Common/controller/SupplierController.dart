import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/model/CommonReqBean.dart';
import 'package:national_tool_app/Pages/Common/model/SupplierRespBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';

class SupplierController extends GetxController {
  final userDetail = GetStorage();

  List<SupplierRespBean> supplier = [];

  // @override
  // onInit() {
  //   fetchAllSupplier().then((value) => supplier = value);
  //   super.onInit();
  // }

  Future<List<SupplierRespBean>> fetchAllSupplier() async {
    CommonReqBean reqBean = CommonReqBean(
        loginUserId: userDetail.read(Finals.USER_ID),
        token: userDetail.read(Finals.TOKEN));
    ResponseBean response = await Apis.callPostService(
        url: Apis.GET_SUPPLIER,
        reqBody: reqBean.toJson(),
        isSuccessBarRequired: false);
    if (response != null && response.responseCode == 0) {
      List<SupplierRespBean> supplierList =
          supplierRespBeanFromJson(response.data);
      supplier = supplierList;
      return supplierList;
    } else
      return [];
  }

  Future<Map<String, int>> get supplierNameIdMap async => Map.fromIterable(await fetchAllSupplier(),
      key: (e) => '${e.name}[${e.suppId}]', value: (e) => e.suppId);
}
