import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/loginPage/controller/LoginController.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/Routes.dart';
import 'package:national_tool_app/Pages/Common/view/commonWidget.dart';

class LoginPage extends StatelessWidget {
  CommonWidget _commonWidget = CommonWidget();
  Size _size;
  final _userName = TextEditingController();
  final _password = TextEditingController();
  LoginController _loginController = Get.find();
  final _userDetail = GetStorage();

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return Scaffold(
      body: Center(
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Container(
                height: _size.height*0.85,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Image.asset(
                      'assets/icon/NTGLogo.png',
                      width: _size.width * 0.6,
                      height: _size.height * 0.25,
                    ),
                    Container(
                      height: _size.height * 0.25,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Login To Your Account',
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          _commonWidget.buildRectTextField(
                              context: context,
                              hintText: 'USER NAME',
                              icon: Icons.person_outline,
                              controller: _userName),
                          _commonWidget.buildRectTextField(
                              context: context,
                              hintText: 'PASSWORD',
                              icon: Icons.remove_red_eye,
                              controller: _password),
                        ],
                      ),
                    ),
                    _commonWidget.buildSquareElevatedButton(
                      context: context,
                      onTap: () => _showLoginPage(context),
                      title: 'login',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _showLoginPage(BuildContext _ctx) {
    _loginController.userLogin(_userName.text, _password.text);
  }
}
