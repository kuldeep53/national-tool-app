import 'dart:convert';

import 'package:get/get.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/controller/SupplierController.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/DrawerPage/controller/MenuController.dart';
import 'package:national_tool_app/Pages/DrawerPage/model/UserMenu.dart';
import 'package:national_tool_app/Pages/loginPage/model/LoginReqBean.dart';
import 'package:national_tool_app/Pages/loginPage/model/LoginRespBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class LoginController extends GetxService {
  final userDetail = GetStorage();

  MenuController _menuController = Get.find();

  static String LOGIN_TOKEN = 'MYRA@@TECHNO';

  Future<LoginRespBean> userLogin(String emailId, String pass) async {
    pass = crypto.md5.convert(utf8.encode(pass)).toString();
    LoginReqBean loginReqBean =
        LoginReqBean(emailId: emailId, password: pass, loginToken: LOGIN_TOKEN);
    ResponseBean response = await Apis.callPostService(
        url: Apis.LOGIN_URL, reqBody: loginReqBean.toJson());
    if (response != null && response.responseCode == 0) {
      LoginRespBean loginRespBean = LoginRespBean.fromJson(response.data);
      userDetail.write(Finals.TOKEN, loginRespBean.token);
      userDetail.write(Finals.NAME, loginRespBean.name);
      userDetail.write(Finals.USER_ID, loginRespBean.userId);
      await _menuController
          .getUserMenus(
              userDetail.read(Finals.TOKEN), userDetail.read(Finals.USER_ID))
          .then((value) => userDetail.write(Finals.MENUS, value));
      Get.toNamed(Routes.DASHBOARD);
      return loginRespBean;
    } else {
      return null;
    }
  }
}
