import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/Common/model/CommonReqBean.dart';
import 'package:national_tool_app/Utility/Apis.dart';
import 'package:national_tool_app/Utility/Finals.dart';

class LogoutController extends GetxController {
  final userDetails = GetStorage();

  @override
  void onInit() {
    logoutUser();
    super.onInit();
  }

  void logoutUser() async {
    CommonReqBean reqBean = CommonReqBean(
        loginUserId: userDetails.read(Finals.USER_ID),
        token: userDetails.read(Finals.TOKEN));
    await Apis.callPostService(
            url: Apis.LOGOUT,
            reqBody: reqBean.toJson(),
            successMessage: 'User Logged Out Successfully')
        .then((value) {
      if (value != null && value.responseCode == 0) {
        GetStorage().erase();
        printInfo(info: 'Get Storage Erased');
      }
    });
  }
}
