// To parse this JSON data, do
//
//     final loginRespBean = loginRespBeanFromJson(jsonString);

import 'dart:convert';

LoginRespBean loginRespBeanFromJson(String str) => LoginRespBean.fromJson(json.decode(str));

String loginRespBeanToJson(LoginRespBean data) => json.encode(data.toJson());

class LoginRespBean {
  LoginRespBean({
    this.userId,
    this.name,
    this.token,
    this.email,
    this.mobileNo,
    this.status,
    this.device,
  });

  var userId;
  String name;
  String token;
  String email;
  String mobileNo;
  String status;
  String device;

  factory LoginRespBean.fromJson(Map<String, dynamic> json) => LoginRespBean(
    userId: json["userId"],
    name: json["name"],
    token: json["token"],
    email: json["email"],
    mobileNo: json["mobileNo"],
    status: json["status"],
    device: json["device"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId,
    "name": name,
    "token": token,
    "email": email,
    "mobileNo": mobileNo,
    "status": status,
    "device": device,
  };
}
