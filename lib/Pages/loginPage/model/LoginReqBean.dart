// To parse this JSON data, do
//
//     final loginReqBean = loginReqBeanFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/foundation.dart';



LoginReqBean loginReqBeanFromJson(String str) =>
    LoginReqBean.fromJson(json.decode(str));

String loginReqBeanToJson(LoginReqBean data) => json.encode(data.toJson());

class LoginReqBean {
  LoginReqBean(
      {this.device = 'ANDROID',
      @required this.emailId,
      @required this.loginToken,
      @required this.password});

  String device;
  String emailId;
  String loginToken;
  String mobileNo;
  String password;

  factory LoginReqBean.fromJson(Map<String, dynamic> json) => LoginReqBean(
        device: json["device"],
        emailId: json["emailId"],
        loginToken: json["loginToken"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "device": device,
        "emailId": emailId,
        "loginToken": loginToken,
        "password": password,
      };
}



