import 'package:get/get.dart';
import 'package:national_tool_app/Pages/Common/controller/SupplierController.dart';
import 'package:national_tool_app/Pages/Common/controller/VariationController.dart';
import 'package:national_tool_app/Pages/DashboardPage/controller/DashBoardController.dart';
import 'package:national_tool_app/Pages/DrawerPage/controller/MenuController.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Pages/loginPage/controller/LoginController.dart';
import 'package:national_tool_app/Pages/loginPage/controller/LogoutController.dart';

class ControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
    Get.lazyPut(() => MenuController());
    Get.lazyPut(() => SupplierController(), fenix: true);
    Get.lazyPut(() => VariationController(), fenix: true);
    Get.lazyPut(() => LogoutController());
  }
}
