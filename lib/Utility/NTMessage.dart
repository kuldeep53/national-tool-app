class NTMessage {

  static const String COMMON_VALIDATION = 'PLease Provide Valid Value';
  static const String RAW_MATERIAL_TYPE = 'Select Any One Raw Material Type.';
  static const String VARIATION_TYPE = 'Select Any One Variation Code.';
  static const String STATE_NAME = 'Select Any One State Name.';
  static const String SOURCE_TYPE = 'Select Any One Type Of Source.';
  static const String SUPPLIER_NAME = 'Supplier Name Must Required.';
  static const String WEIGHT = 'Please Provide Valid Weight.';
  static const String QUANTITY = 'Please Provide Valid Quantity.';
  static const String USER_NAME = 'Please Provide Valid User Name.';
  static const String EMAILId = 'Please Provide Valid Email Address.';
  static const String PASSWORD = 'Please Provide Valid Password.';
  static const String MOBILENO = 'Please Provide Valid Mobile Number.';
  static const String ADDRESS = 'Please Provide Valid Address.';
  static const String HOLE = 'Please Provide Valid Hole Shape.';
  static const String TYPE = 'Please Provide Valid Type.';
  static const String SIZE = 'Please Provide Valid Size.';
  static const String BRAND = 'Please Provide Valid Brand.';
  static const String CITY = 'Please Provide Valid City.';
}
