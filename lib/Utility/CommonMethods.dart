class CommonMethods{


  static final CommonMethods _commonWidget = CommonMethods._internal();

  factory CommonMethods() {
    return _commonWidget;
  }

  CommonMethods._internal();


  String enumValueToString(Object o) => o.toString().split('.').last;

  T enumValueFromString<T>(String key, Iterable<T> values) => values.firstWhere(
        (v) => v != null && key == enumValueToString(v),
    orElse: () => null,
  );
}