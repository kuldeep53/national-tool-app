import 'package:national_tool_app/Utility/NTMessage.dart';

class ValidationMethods {
  static final ValidationMethods _commonWidget = ValidationMethods._internal();

  factory ValidationMethods() {
    return _commonWidget;
  }

  ValidationMethods._internal();

  onUserName(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.USER_NAME;
  }

  onEmailAddress(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.EMAILId;
  }

  onPassword(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.PASSWORD;
  }

  onMobileNo(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.MOBILENO;
  }

  onAddress(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.ADDRESS;
  }

  onSupplierName(String value) {
    if (value == null || value.toString().isEmpty)
      return NTMessage.SUPPLIER_NAME;
  }

  onWeight(String value) {
    double doubleValue = 0;
    if (value != null) doubleValue = double.parse(value);
    if (value == null || doubleValue <= 0) return NTMessage.WEIGHT;
  }

  onQuantity(String value) {
    int intValue = 0;
    if (value != null) intValue = int.parse(value);
    if (value == null || intValue <= 0) return NTMessage.QUANTITY;
  }

  onMaterialType(String value) {
    if (value == null || value.isEmpty) return NTMessage.RAW_MATERIAL_TYPE;
  }

  onVariationType(String value) {
    if (value == null || value.isEmpty) return NTMessage.VARIATION_TYPE;
  }

  onStateName(String value) {
    if (value == null || value.isEmpty) return NTMessage.STATE_NAME;
  }

  onSourceType(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }

  onBrand(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }

  onType(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }
  onHole(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }
  onSize(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }

  onCity(String value) {
    if (value == null || value.isEmpty) return NTMessage.SOURCE_TYPE;
  }
}
