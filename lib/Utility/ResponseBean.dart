// To parse this JSON data, do
//
//     final responseBean = responseBeanFromJson(jsonString);

import 'dart:convert';

ResponseBean responseBeanFromJson(String str) =>
    ResponseBean.fromJson(json.decode(str));

String responseBeanToJson(ResponseBean data) => json.encode(data.toJson());

class ResponseBean {
  ResponseBean({
    this.responseMessage,
    this.responseCode,
    this.data,
  });

  String responseMessage;
  int responseCode;
  dynamic data;

  factory ResponseBean.fromJson(Map<String, dynamic> respData) => ResponseBean(
        responseMessage: respData["responseMessage"],
        responseCode: respData["responseCode"],
        data: respData["data"],
      );

  Map<String, dynamic> toJson() => {
        "responseMessage": responseMessage,
        "responseCode": responseCode,
        "data": data,
      };
}
