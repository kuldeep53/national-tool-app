import 'package:flutter/material.dart';

class CounterTextField extends StatefulWidget {
  final int minValue;
  final int maxValue;
  final double width;
  final double height;

  final ValueChanged<int> onChanged;

  CounterTextField(
      {Key key,
      this.minValue = 0,
      this.maxValue = 10,
      this.onChanged,
      this.width,
      this.height})
      : super(key: key);

  @override
  State<CounterTextField> createState() {
    return _CounterTextField();
  }
}

class _CounterTextField extends State<CounterTextField> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return buildCounterText(context);
  }

  Container buildCounterText(BuildContext context) {
    return Container(
      height: widget.height,
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        border: Border.all(
          color: Theme.of(context).primaryColor,
          width: 2.5,
        ),
      ),
      width: widget.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            icon: Icon(
              Icons.remove,
              color: Theme.of(context).accentColor,
            ),
            // padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 18.0),
            iconSize: 32.0,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              setState(() {
                if (counter > widget.minValue) {
                  counter--;
                }
                widget.onChanged(counter);
              });
            },
          ),
          Text(
            '$counter',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black87,
              fontSize: 18.0,
              fontWeight: FontWeight.w500,
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.add,
              color: Theme.of(context).accentColor,
            ),
            // padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 18.0),
            iconSize: 32.0,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              setState(() {
                if (counter < widget.maxValue) {
                  counter++;
                }
                widget.onChanged(counter);
              });
            },
          ),
        ],
      ),
    );
  }

  Container buildFormFieldCounter(BuildContext context) {
    return Container(
      width: 300,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            icon: Icon(
              Icons.remove,
              color: Theme.of(context).accentColor,
            ),
            padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 18.0),
            iconSize: 32.0,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              setState(() {
                if (counter > widget.minValue) {
                  counter--;
                }
                widget.onChanged(counter);
              });
            },
          ),
          Container(
            width: 80,
            child: TextFormField(
              initialValue: '$counter',
              textAlign: TextAlign.center,
              decoration: InputDecoration(
                // hintText: '$counter',
                hintStyle: TextStyle(fontSize: 14),
                alignLabelWithHint: true,
                border: InputBorder.none,
                // icon: icon == null ? null : Icon(icon)),
              ),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.add,
              color: Theme.of(context).accentColor,
            ),
            padding: EdgeInsets.symmetric(vertical: 4.0, horizontal: 18.0),
            iconSize: 32.0,
            color: Theme.of(context).primaryColor,
            onPressed: () {
              setState(() {
                if (counter < widget.maxValue) {
                  counter++;
                }
                widget.onChanged(counter);
              });
            },
          ),
        ],
      ),
    );
  }
}
