import 'package:flutter/material.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Supplier/SearchSupplierPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/User/SearchUserPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Variation/SearchVariationPage.dart';
import 'package:national_tool_app/Pages/Inventory/view/OpenningInventory/RawMaterial_openning.dart';
import 'package:national_tool_app/Pages/Inventory/view/OpenningInventory/StateCommonOpenning.dart';
import 'package:national_tool_app/Pages/Inventory/view/RawMaterial.dart';
import 'package:national_tool_app/Pages/Inventory/view/StateCommon.dart';
import 'package:national_tool_app/Pages/Reports/view/ItemReportPage.dart';
import 'package:national_tool_app/Pages/Reports/view/ProcessReport.dart';
import 'package:national_tool_app/Pages/Reports/view/RejectionReport.dart';

import '../Pages/Inventory/model/StateEnum.dart';

class Finals {
  /************************Get Storage Keys**********************/
  static const String TOKEN = 'token';
  static const String USER_ID = 'userId';
  static const String NAME = 'name';
  static const String MENUS = 'menus';
  static const String DEVICE = 'ANDROID';

  /**************Menu Icons**********************/
  static const Map<String, IconData> menuIconMap = {
    'DASHBOARD': Icons.dashboard,
    'INVENTORY': Icons.storage,
    'REPORTS': Icons.assessment,
    'CONTROL': Icons.settings,
    'LOGOUT': Icons.logout
  };

  /*****************Graph Colors**********************/
  static const Map<String, Color> graphColors = {
    'Raw Material': Color(0xffc1252e),
    'Cutting': Color(0xffc34b46),
    'Forgging': Color(0xffcc6245),
    'Hardening': Color(0xffcf8963),
    'Repairing': Color(0xffb16e38),
    'Colouring': Color(0xffa66d29),
    'Polishing': Color(0xff765e34),
    'Finishing': Color(0xff766e60)
  };

  static const Map<String, String> stateDescMap = {
    'Raw Material': 'Total Quantatities Of Raw Material',
    'Cutting': 'All Type Of Total Cutting Pieces',
    'Forgging': 'Nos Of Items In Forze state',
    'Hardening': 'Nos Of Items In Hardening state',
    'Repairing': 'Total Nos Of Repaired Items ',
    'Colouring': 'Total Nos Of Colored Pieces',
    'Polishing': 'Total Nos Of Polished Materials',
    'Finishing': 'Finalized Item',
  };

  static const Map<String, String> stateIconMap = {
    'Raw Material': 'assets/icon/raw_material_red.png',
    'Cutting': 'assets/icon/cutting_red.png',
    'Forgging': 'assets/icon/forzing_red.png',
    'Hardening': 'assets/icon/hardening_red.png',
    'Repairing': 'assets/icon/reparing_red.png',
    'Colouring': 'assets/icon/coloring_red.png',
    'Polishing': 'assets/icon/polishing_red.png',
    'Finishing': 'assets/icon/finishing_red.png',
    'Rejection': 'assets/icon/polishing_red.png',
    'Update States': 'assets/icon/polishing_red.png',
  };

  static final Map<String, Widget> inventoryTabs = {
    'Raw Material': RawMaterialPage(),
    'Cutting': StateCommonPage(titleText: States.CUTTING),
    'Forgging': StateCommonPage(titleText: States.FORZING),
    'Hardening': StateCommonPage(titleText: States.HARDENING),
    'Repairing': StateCommonPage(titleText: States.REPARING),
    'Colouring': StateCommonPage(titleText: States.COLORING),
    'Polishing': StateCommonPage(titleText: States.POLISHING),
    'Finishing': StateCommonPage(titleText: States.FINISHING),
    'Rejection': Center(child: Text('REJECTION')),
    'Update States': Center(child: Text('Update state'))
  };


  static final Map<String, Widget> openningInventoryTabs = {
    'Raw Material': RawMaterialOpenningPage(),
    'Cutting': StateCommonOpenningPage(titleText: States.CUTTING),
    'Forgging': StateCommonOpenningPage(titleText: States.FORZING),
    'Hardening': StateCommonOpenningPage(titleText: States.HARDENING),
    'Repairing': StateCommonOpenningPage(titleText: States.REPARING),
    'Colouring': StateCommonOpenningPage(titleText: States.COLORING),
    'Polishing': StateCommonOpenningPage(titleText: States.POLISHING),
    'Finishing': StateCommonOpenningPage(titleText: States.FINISHING),
  };

  static final Map<String, Widget> reportTabs = {
    'PROCESS_REPORT': ProcessReport(),
    'ORDER_HISTORY': Center(child: Text('ORDER HISTORY')),
    'REJECTION_REPORT': RejectionReport(),
    'ITEM_REPORT': ItemReportPage(),
  };

  static final Map<String, IconData> reportIconMap = {
    'PROCESS_REPORT': Icons.assessment,
    'ORDER_HISTORY': Icons.description,
    'REJECTION_REPORT': Icons.favorite_rounded,
    'ITEM_REPORT': Icons.dock_outlined,
  };

  static final Map<String, Widget> controlPanelTabs = {
    'User Search': SearchUserPage(),
    'Search Item': SearchVariationPage(),
    'CLients': Center(child: Text('ORDER1 HISTORY')),
    'Search Suppliers': SearchSupplierPage(),
    'States': Center(child: Text('ORDER3 HISTORY')),
  };

  static final Map<String, String> controlPanelTabName = {
    'User Search': 'Users',
    'Search Item': 'Variations',
    'CLients': 'Clients',
    'Search Suppliers': 'Suppliers',
  };

  static final Map<String, IconData> controlPanelIconMap = {
    'User Search': Icons.eighteen_mp,
    'Search Item': Icons.assessment,
    'Search Suppliers': Icons.description,
    'Suppliers': Icons.favorite_rounded,
    'States': Icons.favorite_rounded,
  };
}
