import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Pages/ControlPanel/controller/ControlPanelController.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Supplier/AddSupplierPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Supplier/UpdateSupplierPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/User/AddUserPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/ContronBottomTabBar.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/User/AssignMenuPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/User/UpdateUserPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Variation/AddVariationPage.dart';
import 'package:national_tool_app/Pages/ControlPanel/view/Variation/UpdateVariationPage.dart';
import 'package:national_tool_app/Pages/DashboardPage/controller/DashBoardController.dart';
import 'package:national_tool_app/Pages/DashboardPage/view/DashBoard.dart';
import 'package:national_tool_app/Pages/DashboardPage/view/GraphExample.dart';
import 'package:national_tool_app/Pages/Inventory/controller/InventoryTabController.dart';
import 'package:national_tool_app/Pages/Inventory/controller/OpenningInventoryTabController.dart';
import 'package:national_tool_app/Pages/Inventory/view/InventoryBottomTabBar.dart';
import 'package:national_tool_app/Pages/Inventory/view/OpenningInventory/OpenningInventoryBottomTabBar.dart';
import 'package:national_tool_app/Pages/Inventory/view/RawMaterial.dart';
import 'package:national_tool_app/Pages/Inventory/view/rejection_page.dart';
import 'package:national_tool_app/Pages/Inventory/view/update_state.dart';
import 'package:national_tool_app/Pages/Reports/controller/ReportController.dart';
import 'package:national_tool_app/Pages/Reports/view/ReportBottomTabBar.dart';
import 'package:national_tool_app/Pages/loginPage/controller/LoginController.dart';
import 'package:national_tool_app/Pages/loginPage/controller/LogoutController.dart';
import '../Pages/loginPage/view/LoginPage.dart';

class Routes {
  static const BASE_URL = '/';
  static const DASHBOARD = '/dashboard';
  static const INVENTORY = '/inventory';
  static const REPORT = '/reports';
  static const CONTROL = '/control';
  static const LOGOUT = '/login';
  static const ADD_USER = '/addUser';
  static const UPDATE_USER = '/updateUser';
  static const ASSIGN_MENUS = '/assignMenus';
  static const REJECTION = '/rejection';
  static const UPDATE_STATE = '/update';
  static const UPDATE_VARIATIONS = '/updateVariation';
  static const ADD_VARIATIONS = '/addVariation';
  static const ADD_SUPPLIER = '/addSupplier';
  static const UPDATE_SUPPLIER = '/updateSupplier';
  static const OPENNING_INVENTORY = '/openingInventory';

  Map<String, WidgetBuilder> test;

  static final routes = [
    GetPage(
      name: BASE_URL,
      page: () => LoginPage(),
      binding: BindingsBuilder.put(() => LoginController()),
    ),
    GetPage(
      name: DASHBOARD,
      page: () => DashBoard(),
      binding: BindingsBuilder.put(() => DashboardController()),
    ),
    GetPage(
      name: INVENTORY,
      page: () => InventoryBottomTabBar(),
      binding: BindingsBuilder.put(() => InventoryTabController()),
    ),
    GetPage(
      name: OPENNING_INVENTORY,
      page: () => OpenningInventoryBottomTabBar(),
      binding:
          BindingsBuilder.put(() => OpenningInventoryTabController()),
    ),
    GetPage(
      name: REPORT,
      page: () => ReportBottomTabBar(),
      binding: BindingsBuilder.put(() => ReportController()),
    ),
    GetPage(
      name: CONTROL,
      page: () => ControlBottomTabBar(),
      binding: BindingsBuilder.put(() => ControlPanelController()),
    ),
    GetPage(
      name: LOGOUT,
      page: () => LoginPage(),
      binding: BindingsBuilder.put(() => LogoutController()),
    ),
    GetPage(
      name: ADD_USER,
      page: () => AddUserPage(),
    ),
    GetPage(
      name: UPDATE_USER,
      page: () => UpdateUserPage(),
    ),
    GetPage(
      name: ASSIGN_MENUS,
      page: () => AssignMenuPage(),
    ),
    GetPage(
      name: REJECTION,
      page: () => RejectionPage(),
      binding: BindingsBuilder.put(() => InventoryTabController()),
    ),
    GetPage(
      name: UPDATE_STATE,
      page: () => UpdateState(),
      binding: BindingsBuilder.put(() => InventoryTabController()),
    ),
    GetPage(
      name: UPDATE_VARIATIONS,
      page: () => UpdateVariationPage(),
    ),
    GetPage(
      name: ADD_VARIATIONS,
      page: () => AddVariationPage(),
    ),
    GetPage(
      name: ADD_SUPPLIER,
      page: () => AddSupplierPage(),
    ),
    GetPage(
      name: UPDATE_SUPPLIER,
      page: () => UpdateSupplierPage(),
    ),
  ];
}
