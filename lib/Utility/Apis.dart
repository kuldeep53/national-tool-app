import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:national_tool_app/Utility/ResponseBean.dart';
import 'package:national_tool_app/Utility/Routes.dart';

class Apis {
  static const String BASE_URL = 'http://65.2.43.217:3005/api/';

  // ignore: missing_return
  static Future<ResponseBean> callPostService({
    @required String url,
    @required Map<String, dynamic> reqBody,
    String successMessage = 'Transaction Successfully',
    bool isSuccessBarRequired = true,
  }) async {
    try {
      Get.printInfo(
          info: '${url.substring(url.lastIndexOf("/") + 1)} '
              'reqBean:---${reqBody}');
      final response =
          await GetConnect().post(url, reqBody).timeout(Duration(seconds: 10));
      Get.printInfo(
          info: '${url.substring(url.lastIndexOf("/") + 1)} '
              'response:---${response.bodyString}');
      if (response?.status?.hasError) {
        Get.printInfo(info: 'error');
        Future.error(response.statusText);
      } else {
        Get.printInfo(info: response?.bodyString);
        ResponseBean responseBean = responseBeanFromJson(response?.bodyString);
        if (responseBean?.responseCode == 0) {
          isSuccessBarRequired
              ? Get.snackbar('SUCCESS', successMessage,
                  margin: EdgeInsets.only(top: 10),
                  colorText: Color(0xff07720b),
                  dismissDirection: SnackDismissDirection.HORIZONTAL)
              : Get.printInfo(info: 'SuccessFully Executed ${url}');
          return responseBean;
        } else if (responseBean.responseCode == 1104) {
          GetStorage().erase();
          Get.toNamed(Routes.BASE_URL);
        } else {
          Get.snackbar('FAILED', '',
              messageText: Text(
                '${responseBean?.responseMessage}',
                style: TextStyle(
                    fontSize: 14,
                    color: Color(0xffa50505),
                    fontWeight: FontWeight.w500),
              ),
              margin: EdgeInsets.only(top: 10),
              colorText: Color(0xffa50505),
              dismissDirection: SnackDismissDirection.HORIZONTAL);
          Future.error(responseBean?.responseMessage);
        }
      }
    } catch (e) {
      Get.printError(info: 'Something went wrong');
      Get.snackbar('FAILED', '',
          messageText: Text(
            '${e}',
            style: TextStyle(
                fontSize: 14,
                color: Color(0xffa50505),
                fontWeight: FontWeight.w500),
          ),
          margin: EdgeInsets.only(top: 10),
          colorText: Color(0xffa50505),
          dismissDirection: SnackDismissDirection.HORIZONTAL);
      Future.error(e.toString());
    }finally{
      Get.printInfo(info: 'Url Name;---${url}');
    }
  }

  static const String LOGIN_URL = BASE_URL + 'login';
  static const String GET_USER_MENU = BASE_URL + 'getUserMenus';
  static const String GET_STATE_DETAIL = BASE_URL + 'getStateDetails';
  static const String ADD_RAW_MATERIAL = BASE_URL + 'addRawMaterials';
  static const String GET_SUPPLIER = BASE_URL + 'getSupplier';
  static const String LOGOUT = BASE_URL + 'logout';
  static const String ADD_CUTTING = BASE_URL + 'addCutting';
  static const String ADD_FORZING = BASE_URL + 'addFrogging';
  static const String ADD_COLORING = BASE_URL + 'addColouring';
  static const String ADD_FINISHING = BASE_URL + 'addFinishing';
  static const String ADD_HARDENING = BASE_URL + 'addHardening';
  static const String ADD_POLISHING = BASE_URL + 'addPolishing';
  static const String ADD_REJECTION = BASE_URL + 'addRejection';
  static const String ADD_REPARING = BASE_URL + 'addRepairing';
  static const String GET_VARIATION = BASE_URL + 'getVariation';
  static const String GET_PROCESS_HISTORY = BASE_URL + 'getProcessHistory';
  static const String GET_REJECTION_HISTORY = BASE_URL + 'getRejection';
  static const String SEARCH_USER = BASE_URL + 'searchUser';
  static const String REGISTER_USER = BASE_URL + 'registerUser';
  static const String UPDATE_USER = BASE_URL + 'updateUser';
  static const String UPDATE_VARIATION = BASE_URL + 'updateVariation';
  static const String ADD_VARIATION = BASE_URL + 'addVariation';
  static const String GET_ASSIGNED_MENUS = BASE_URL + 'getAssigenedMenus';
  static const String ASSIGN_MENU_TO_USER = BASE_URL + 'assignMenuToUser';
  static const String GET_VARIATION_DETAIL = BASE_URL + 'getVariationDetail';
  static const String GET_CUTTING = BASE_URL + 'getCutting';
  static const String GET_FORZING = BASE_URL + 'getFrogging';
  static const String GET_COLORING = BASE_URL + 'getColouring';
  static const String GET_FINISHING = BASE_URL + 'getFinishing';
  static const String GET_HARDENING = BASE_URL + 'getHardening';
  static const String GET_POLISHING = BASE_URL + 'getPolishing';
  static const String GET_REPARING = BASE_URL + 'getRepairing';
  static const String ADD_SUPPLIER = BASE_URL + 'addSupplier';
  static const String UPDATE_SUPPLIER = BASE_URL + 'updateSupplier';

}
